Mise en place d'un système de gestion de bibliothèque : le but est de concevoir une solution e-gestion e-bibliothèque exploitant une application mobile, des QRCodes et une API Rest

Technologies utilisées :
- IDE Android Studio
- Java
- Node JS (Express)
- NPM
- TypeScript
- MongoDB (NoSQL)
