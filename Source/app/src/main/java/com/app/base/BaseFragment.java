package com.app.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.home.HomeActivity;

import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

public abstract class BaseFragment extends Fragment {
    public boolean onBackPressed() {
        return true;
    }

    public void remoteCall(Bundle bundle) {
    }

    protected void onViewCompleteLoaded() {
    }

    protected abstract int getLayout();

    protected abstract int getTitle();

    protected abstract void afterActivityCreated();

    protected HomeActivity homeActivity;

    protected void setToolbarTitle(String title) {
        homeActivity.setToolbarTitle(title);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof HomeActivity) {
            this.homeActivity = (HomeActivity) context;
            homeActivity.setToolbarTitle(getTitle());
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            if (homeActivity != null) {
                homeActivity.setToolbarTitle(getTitle());
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayout(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                BaseFragment.this.onViewCompleteLoaded();
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        afterActivityCreated();
    }

    private ActivityResultListener activityResultListener;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        int PICK_IMAGE = 100;

        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            if (data != null) {
                if (activityResultListener != null) {
                    activityResultListener.onResult(data);
                }
            }
        }
    }

    public void callActivityForResult(Intent intent, int requestCode, ActivityResultListener activityResultListener) {
        this.activityResultListener = activityResultListener;
        startActivityForResult(intent, requestCode);
    }

    public interface ActivityResultListener {
        void onResult(Intent Intent);
    }
}
