package com.app.category.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.R;
import com.app.category.holder.CategoryListingHolder;
import com.app.databinding.ObjectCategoryBinding;
import com.app.retrofit.model.object.Category;
import com.app.retrofit.model.response.ListCategoryResponse;
import com.app.retrofit.utils.EntityUtils;
import com.app.view.PaginationAdapter;

import io.reactivex.Observable;

public class CategoryListingAdapter extends PaginationAdapter<ListCategoryResponse, CategoryListingHolder> {
    private final CategoryListingHolder.CategoryListingHolderListener categoryListingHolderListener;

    private final boolean deleteMode;

    public CategoryListingAdapter(Context context, SwipeRefreshLayout swipeRefreshLayout, boolean deleteMode, CategoryListingHolder.CategoryListingHolderListener categoryListingHolderListener) {
        super(context, swipeRefreshLayout);
        this.deleteMode = deleteMode;
        this.categoryListingHolderListener = categoryListingHolderListener;

        start(this);
    }

    @NonNull
    @Override
    public CategoryListingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ObjectCategoryBinding objectCategoryBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.object_category, parent, false);
        return new CategoryListingHolder(objectCategoryBinding, categoryListingHolderListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryListingHolder holder, int position) {
        holder.bind((Category) entities.get(position), deleteMode);
    }

    @Override
    protected Observable<ListCategoryResponse> getObservable() {
        return EntityUtils.getCategoriesRx(currentPage, EntityUtils.LIMIT_GET_CATEGORIES_PER_PAGE, keyword);
    }
}
