package com.app.category.holder;

import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.R;
import com.app.databinding.ObjectCategoryBinding;
import com.app.dialog.PopupConfirmCancel;
import com.app.retrofit.model.object.Category;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryListingHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_delete)
    ImageView ivDelete;

    private final ObjectCategoryBinding objectCategoryBinding;
    private final CategoryListingHolderListener categoryListingHolderListener;

    public CategoryListingHolder(ObjectCategoryBinding objectCategoryBinding, CategoryListingHolderListener categoryListingHolderListener) {
        super(objectCategoryBinding.getRoot());
        ButterKnife.bind(this, itemView);
        this.objectCategoryBinding = objectCategoryBinding;
        this.categoryListingHolderListener = categoryListingHolderListener;
    }

    public void bind(Category category, boolean deleteMode) {
        objectCategoryBinding.setCategory(category);
        objectCategoryBinding.setDelete(deleteMode);
        itemView.setOnClickListener(v -> categoryListingHolderListener.onCategoryClick(category));
        ivDelete.setOnClickListener(v -> requestCategoryDelete(category));
    }

    private void requestCategoryDelete(Category category) {
        PopupConfirmCancel popupConfirmCancel = PopupConfirmCancel.getInstance(itemView.getContext(), R.string.confirm_delete_category, () -> categoryListingHolderListener.onCategoryDelete(category));
        popupConfirmCancel.show();
    }

    public interface CategoryListingHolderListener {
        void onCategoryClick(Category category);

        void onCategoryDelete(Category category);
    }
}
