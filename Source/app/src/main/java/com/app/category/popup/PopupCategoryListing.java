package com.app.category.popup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.R;
import com.app.category.adapter.CategoryListingAdapter;
import com.app.category.holder.CategoryListingHolder;
import com.app.dialog.helper.DialogHelper;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.model.decoration.MarginItemDecoration;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Category;
import com.app.retrofit.model.response.DeleteCategoryResponse;
import com.app.retrofit.utils.ResponseUtils;
import com.app.utils.CallbackEntity;
import com.app.utils.ViewUtils;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import retrofit2.Call;

public class PopupCategoryListing extends Dialog implements CategoryListingHolder.CategoryListingHolderListener {
    @BindView(R.id.et_search_bar)
    EditText etSearchBar;
    @BindView(R.id.iv_create_category)
    ImageView ivCreateCategory;
    @BindView(R.id.rv_categories)
    RecyclerView rvCategories;
    @BindView(R.id.btn_back)
    Button btnBack;

    private Context context;

    private final boolean deleteMode;
    private final boolean createMode;

    private CategoryListingAdapter categoryListingAdapter;

    @SuppressLint("StaticFieldLeak")
    private static PopupCategoryListing popupCategoryListing;

    private PopupCategoryListing(Context context, boolean deleteMode, boolean createMode) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
        this.deleteMode = deleteMode;
        this.createMode = createMode;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_category_listing, null);

        ButterKnife.bind(this, view);
        setContentView(view);

        this.context = getContext();

        initView();
    }

    @Override
    public void dismiss() {
        popupCategoryListing = null;
        rvCategories.setAdapter(null);
        super.dismiss();
    }

    public static PopupCategoryListing getInstance(Context context, boolean deleteMode, boolean createMode) {
        if (popupCategoryListing == null) {
            popupCategoryListing = new PopupCategoryListing(context, deleteMode, createMode);
        }
        return popupCategoryListing;
    }

    private void initView() {
        categoryListingAdapter = new CategoryListingAdapter(context, null, deleteMode, this);
        rvCategories.setLayoutManager(new LinearLayoutManager(context));
        rvCategories.addItemDecoration(new MarginItemDecoration(2));
        rvCategories.setAdapter(categoryListingAdapter);

        if (createMode) {
            ivCreateCategory.setOnClickListener(v -> {
                PopupCreateCategory popupCreateCategory = PopupCreateCategory.getInstance(context, category -> etSearchBar.setText(""));
                popupCreateCategory.show();
            });
        } else {
            ivCreateCategory.setVisibility(View.GONE);
        }

        btnBack.setOnClickListener(v -> dismiss());
    }

    @OnTextChanged(R.id.et_search_bar)
    public void searchBarTextChanged() {
        categoryListingAdapter.refreshKeyword(etSearchBar.getText().toString());
    }

    @Override
    public void onCategoryClick(Category category) {

    }

    @Override
    public void onCategoryDelete(Category category) {
        DialogHelper.getInstance().showLoading(context);

        Call<DeleteCategoryResponse> call = RetrofitInstance.getAPIService().deleteCategoryById(category.getId());
        call.enqueue(new CallbackEntity<>(DeleteCategoryResponse.class, new CallbackEntity.CallbackEntityResponse<DeleteCategoryResponse>() {
            @Override
            public void onSuccess(DeleteCategoryResponse deleteCategoryResponse) {
                DialogHelper.getInstance().dismissDialog();
                if (deleteCategoryResponse.getTotal_deleted() != null && deleteCategoryResponse.getTotal_deleted() > 0) {
                    if (categoryListingAdapter.remove(category)) {
                        ViewUtils.toast(context, R.string.category_deleted, false);
                    } else {
                        ResponseUtils.onCallFailed(context);
                    }
                }
            }

            @Override
            public void onFailure() {
                DialogHelper.getInstance().dismissDialog();
                ResponseUtils.onCallFailed(context);
            }
        }, ResponseCodeHttpConstant.SUCCESS));
    }
}


































