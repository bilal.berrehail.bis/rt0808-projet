package com.app.category.popup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.R;
import com.app.dialog.helper.DialogHelper;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.model.fields.CategoryFields;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Category;
import com.app.retrofit.model.response.CategoryResponse;
import com.app.utils.CallbackEntity;
import com.app.utils.ViewUtils;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class PopupCreateCategory extends Dialog {
    @BindView(R.id.et_category_name)
    EditText etCategoryName;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_create)
    Button btnCreate;

    private final Context context;

    private final CreateCategoryListener createCategoryListener;

    @SuppressLint("StaticFieldLeak")
    private static PopupCreateCategory popupCreateCategory;

    private PopupCreateCategory(Context context, CreateCategoryListener createCategoryListener) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
        this.createCategoryListener = createCategoryListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_create_category, null);

        ButterKnife.bind(this, view);

        setContentView(view);
        initView();
    }

    @Override
    public void dismiss() {
        popupCreateCategory = null;
        super.dismiss();
    }

    private void initView() {
        btnCancel.setOnClickListener(v -> dismiss());
        btnCreate.setOnClickListener(v -> {
            Category newCategory = new Category();
            newCategory.setLabel(etCategoryName.getText().toString());

            DialogHelper.getInstance().showLoading(context);

            HashMap<String, Object> mapCategory = CategoryFields.serializeForCreation(newCategory);

            Call<CategoryResponse> call = RetrofitInstance.getAPIService().createCategory(mapCategory);
            call.enqueue(new CallbackEntity<>(CategoryResponse.class, new CallbackEntity.CallbackEntityResponse<CategoryResponse>() {
                @Override
                public void onSuccess(CategoryResponse categoryResponse) {
                    DialogHelper.getInstance().dismissDialog();

                    Category category = categoryResponse.getEntity();
                    newCategory.setId(category.getId());

                    ViewUtils.toast(context, R.string.category_created, false);
                    createCategoryListener.onSuccess(category);
                    dismiss();
                }

                @Override
                public void onFailure() {
                    DialogHelper.getInstance().dismissDialog();
                    ViewUtils.toast(context, R.string.error_create_category, true);
                    dismiss();
                }
            }, ResponseCodeHttpConstant.CREATE_SUCCESS));
        });
    }

    public static PopupCreateCategory getInstance(Context context, CreateCategoryListener createCategoryListener) {
        if (popupCreateCategory == null) {
            popupCreateCategory = new PopupCreateCategory(context, createCategoryListener);
        }
        return popupCreateCategory;
    }

    public interface CreateCategoryListener {
        void onSuccess(Category category);
    }
}
