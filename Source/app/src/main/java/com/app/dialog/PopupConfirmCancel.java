package com.app.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.R;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PopupConfirmCancel extends Dialog {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;

    private final int resTitle;

    private final Context context;
    private final Response response;

    @SuppressLint("StaticFieldLeak")
    private static PopupConfirmCancel popupConfirmCancel;

    private PopupConfirmCancel(Context context, int resTitle, Response response) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
        this.resTitle = resTitle;
        this.response = response;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_choice, null);

        ButterKnife.bind(this, view);

        setContentView(view);
        initView();
    }

    @Override
    public void dismiss() {
        popupConfirmCancel = null;
        super.dismiss();
    }

    public static PopupConfirmCancel getInstance(Context context, int resTitle, Response response) {
        if (popupConfirmCancel == null) {
            popupConfirmCancel = new PopupConfirmCancel(context, resTitle, response);
        }
        return popupConfirmCancel;
    }

    private void initView() {
        tvTitle.setText(resTitle);
        btnCancel.setOnClickListener(v -> dismiss());
        btnConfirm.setOnClickListener(v -> {
            response.onConfirm();
            dismiss();
        });
    }

    public interface Response {
        void onConfirm();
    }
}
