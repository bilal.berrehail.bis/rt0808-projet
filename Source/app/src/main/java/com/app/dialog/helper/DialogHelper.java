package com.app.dialog.helper;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

import com.app.R;

public class DialogHelper {
    private static DialogHelper dialogHelper;
    private Dialog dialog;

    public static DialogHelper getInstance() {
        if (dialogHelper == null) {
            dialogHelper = new DialogHelper();
        }
        return dialogHelper;
    }

    public void showLoading(Context context) {
        if (dialog != null) {
            return;
        }

        dialog = new ProgressDialog(context, R.style.LoadingDialogTheme);
        ((ProgressDialog) dialog).setMessage(context.getString(R.string.loading));
        ((ProgressDialog) dialog).setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.show();
    }

    public void dismissDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }
}
