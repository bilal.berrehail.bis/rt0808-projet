package com.app.exemplary.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.R;
import com.app.databinding.ObjectExemplaryListingBinding;
import com.app.exemplary.holder.ExemplaryListingHolder;
import com.app.retrofit.model.object.Exemplary;
import com.app.retrofit.model.response.ListExemplaryResponse;
import com.app.retrofit.utils.EntityUtils;
import com.app.view.PaginationAdapter;

import io.reactivex.Observable;

public class ExemplaryListingAdapter extends PaginationAdapter<ListExemplaryResponse, ExemplaryListingHolder> {
    private final boolean moveMode;
    private final boolean lendMode;

    private final ExemplaryListingHolder.ExemplaryListingListener exemplaryListingListener;

    public ExemplaryListingAdapter(Context context, SwipeRefreshLayout swipeRefreshLayout, boolean moveMode, boolean lendMode, ExemplaryListingHolder.ExemplaryListingListener exemplaryListingListener) {
        super(context, swipeRefreshLayout);
        this.moveMode = moveMode;
        this.lendMode = lendMode;
        this.exemplaryListingListener = exemplaryListingListener;

        start(this);
    }

    @NonNull
    @Override
    public ExemplaryListingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ObjectExemplaryListingBinding objectExemplaryListingBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.object_exemplary_listing, parent, false);
        return new ExemplaryListingHolder(objectExemplaryListingBinding, exemplaryListingListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ExemplaryListingHolder holder, int position) {
        holder.bind((Exemplary) entities.get(position), moveMode, lendMode);
    }

    @Override
    protected Observable<ListExemplaryResponse> getObservable() {
        return EntityUtils.getExemplariesRx(currentPage, EntityUtils.LIMIT_GET_EXEMPLARIES_ITEM_PER_PAGE, keyword);
    }
}
