package com.app.exemplary.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.R;
import com.app.databinding.ObjectStockListingBinding;
import com.app.exemplary.holder.ExemplaryLocationListingHolder;
import com.app.retrofit.model.object.Exemplary;
import com.app.retrofit.model.response.ListExemplaryResponse;
import com.app.retrofit.utils.EntityUtils;
import com.app.view.PaginationAdapter;

import io.reactivex.Observable;
import io.reactivex.Observer;

public class ExemplaryLocationListingAdapter extends PaginationAdapter<ListExemplaryResponse, ExemplaryLocationListingHolder> {
    private final String location_id;
    private final ExemplaryLocationListingHolder.ExemplaryLocationListingListener exemplaryLocationListingListener;

    public ExemplaryLocationListingAdapter(Context context, SwipeRefreshLayout swipeRefreshLayout, String location_id, ExemplaryLocationListingHolder.ExemplaryLocationListingListener exemplaryLocationListingListener) {
        super(context, swipeRefreshLayout);
        this.location_id = location_id;
        this.exemplaryLocationListingListener = exemplaryLocationListingListener;

        start(this);
    }

    @NonNull
    @Override
    public ExemplaryLocationListingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ObjectStockListingBinding objectStockListingBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.object_stock_listing, parent, false);
        return new ExemplaryLocationListingHolder(objectStockListingBinding, exemplaryLocationListingListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ExemplaryLocationListingHolder holder, int position) {
        holder.bind((Exemplary) entities.get(position));
    }

    @Override
    protected Observable<ListExemplaryResponse> getObservable() {
        return EntityUtils.getExemplariesByLocationIdRx(currentPage, EntityUtils.LIMIT_GET_EXEMPLARIES_LOCATION_PER_PAGE, keyword, location_id);
    }
}
