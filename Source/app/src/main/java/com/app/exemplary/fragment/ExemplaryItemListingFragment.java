package com.app.exemplary.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.R;
import com.app.base.BaseFragment;
import com.app.databinding.FragmentExemplaryListingBinding;
import com.app.exemplary.adapter.ExemplaryItemListingAdapter;
import com.app.exemplary.holder.ExemplaryListingHolder;
import com.app.exemplary.popup.PopupCreateExemplary;
import com.app.exemplary.popup.PopupExemplaryDetail;
import com.app.lend.fragment.LendFragment;
import com.app.model.constant.BundleConstant;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.model.decoration.MarginItemDecoration;
import com.app.move.MoveFragment;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Exemplary;
import com.app.retrofit.model.object.Item;
import com.app.retrofit.model.response.ItemResponse;
import com.app.retrofit.utils.ResponseUtils;
import com.app.scanner.PopupScan;
import com.app.utils.BundleUtils;
import com.app.utils.CallbackEntity;
import com.app.utils.DataScanUtils;
import com.app.utils.ViewUtils;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.OnTextChanged;
import retrofit2.Call;

public class ExemplaryItemListingFragment extends BaseFragment implements ExemplaryListingHolder.ExemplaryListingListener, Toolbar.OnMenuItemClickListener {
    @BindView(R.id.et_search_bar)
    EditText etSearchBar;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.rv_exemplary)
    RecyclerView rvExemplarys;

    private Item item;

    private PopupScan popupScan;

    private ExemplaryItemListingAdapter exemplaryItemListingAdapter;

    private FragmentExemplaryListingBinding fragmentExemplaryListingBinding;

    @Override
    protected int getLayout() {
        return R.layout.fragment_exemplary_listing;
    }

    @Override
    protected int getTitle() {
        return R.string.consultation_exemplaries;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        rvExemplarys.setAdapter(null);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            etSearchBar.setText("");
        }
    }

    @Override
    public void remoteCall(Bundle bundle) {
        if (BundleUtils.getValue(bundle, BundleConstant.BUNDLE_UPDATE) != null) {
            Call<ItemResponse> call = RetrofitInstance.getAPIService().getItemById(item.getId());
            call.enqueue(new CallbackEntity<>(ItemResponse.class, new CallbackEntity.CallbackEntityResponse<ItemResponse>() {
                @Override
                public void onSuccess(ItemResponse itemResponse) {
                    ExemplaryItemListingFragment.this.item = itemResponse.getEntity();
                    fragmentExemplaryListingBinding.setItem(item);
                }

                @Override
                public void onFailure() {
                    ResponseUtils.onCallFailed(homeActivity);
                }
            }, ResponseCodeHttpConstant.SUCCESS));

            etSearchBar.setText("");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        this.item = new Gson().fromJson((String) BundleUtils.getValue(getArguments(), BundleConstant.BUNDLE_ITEM), Item.class);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        homeActivity.inflateToolbarMenu(R.menu.menu_exemplary_item_listing, this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentExemplaryListingBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_exemplary_listing, container, false);
        View root = fragmentExemplaryListingBinding.getRoot();
        fragmentExemplaryListingBinding.setItem(item);

        return root;
    }

    @Override
    protected void afterActivityCreated() {
        initExemplaryItemAdapter();
    }

    private void initExemplaryItemAdapter() {
        exemplaryItemListingAdapter = new ExemplaryItemListingAdapter(homeActivity, item.getId(), swipeRefresh, true, true, this);

        rvExemplarys.setLayoutManager(new LinearLayoutManager(homeActivity));
        rvExemplarys.addItemDecoration(new MarginItemDecoration(8));
        rvExemplarys.setAdapter(exemplaryItemListingAdapter);
    }

    private void handleScanExemplary() {
        popupScan = PopupScan.getInstance(homeActivity, result -> {
            DataScanUtils.buildExemplary(result.getText(), new DataScanUtils.DataScanUtilsListener() {
                @Override
                public void onBuildExemplary(Exemplary exemplary) {
                    if (exemplary.getLabel() != null) {
                        etSearchBar.setText(exemplary.getLabel());
                    }
                }

                @Override
                public void onBuildFailed() {
                    ViewUtils.toast(homeActivity, R.string.no_exemplary_found, true);
                }
            });
            popupScan.dismiss();
        });
        popupScan.show();
    }

    @OnTextChanged(R.id.et_search_bar)
    public void searchBarTextChanged() {
        exemplaryItemListingAdapter.refreshKeyword(etSearchBar.getText().toString());
    }

    @Override
    public void onClick(Exemplary exemplary) {
        exemplary.setItem(item);
        PopupExemplaryDetail popupExemplaryDetail = PopupExemplaryDetail.getInstance(homeActivity, exemplary);
        popupExemplaryDetail.show();
    }

    @Override
    public void onLend(Exemplary exemplary) {
        LendFragment lendFragment = new LendFragment();

        Bundle b = new Bundle();
        b.putString(BundleConstant.BUNDLE_EXEMPLARY, new Gson().toJson(exemplary));
        lendFragment.setArguments(b);

        homeActivity.pushFragment(lendFragment);
    }

    @Override
    public void onMoveRequest(Exemplary exemplary) {
        MoveFragment moveFragment = new MoveFragment();

        Bundle b = new Bundle();
        b.putString(BundleConstant.BUNDLE_EXEMPLARY, new Gson().toJson(exemplary));
        moveFragment.setArguments(b);

        homeActivity.pushFragment(moveFragment);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                PopupCreateExemplary popupCreateExemplary = PopupCreateExemplary.getInstance(homeActivity, this.item.getId(), exemplary -> etSearchBar.setText(""));
                popupCreateExemplary.show();
                return true;
            case R.id.action_scan:
                handleScanExemplary();
                return true;
        }
        return false;
    }
}















