package com.app.exemplary.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.R;
import com.app.base.BaseFragment;
import com.app.databinding.FragmentStockListingBinding;
import com.app.exemplary.adapter.ExemplaryLocationListingAdapter;
import com.app.exemplary.holder.ExemplaryLocationListingHolder;
import com.app.exemplary.popup.PopupExemplaryDetail;
import com.app.model.constant.BundleConstant;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.model.decoration.MarginItemDecoration;
import com.app.move.MoveFragment;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Exemplary;
import com.app.retrofit.model.object.Location;
import com.app.retrofit.model.response.LocationResponse;
import com.app.retrofit.utils.ResponseUtils;
import com.app.utils.BundleUtils;
import com.app.utils.CallbackEntity;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.OnTextChanged;
import retrofit2.Call;

public class ExemplaryLocationListingFragment extends BaseFragment {
    @BindView(R.id.et_search_bar)
    EditText etSearchBar;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.rv_stocks)
    RecyclerView rvStocks;

    private Location location;

    private ExemplaryLocationListingAdapter exemplaryLocationListingAdapter;

    private FragmentStockListingBinding fragmentStockListingBinding;

    @Override
    protected int getLayout() {
        return R.layout.fragment_stock_listing;
    }

    @Override
    protected int getTitle() {
        return R.string.consultation_stocks;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            etSearchBar.setText("");
        }
    }

    @Override
    public void remoteCall(Bundle bundle) {
        if (BundleUtils.getValue(bundle, BundleConstant.BUNDLE_UPDATE) != null) {

            Call<LocationResponse> call = RetrofitInstance.getAPIService().getLocationById(location.getId());
            call.enqueue(new CallbackEntity<>(LocationResponse.class, new CallbackEntity.CallbackEntityResponse<LocationResponse>() {
                @Override
                public void onSuccess(LocationResponse locationResponse) {
                    ExemplaryLocationListingFragment.this.location = locationResponse.getEntity();
                    fragmentStockListingBinding.setLocation(location);
                }

                @Override
                public void onFailure() {
                    ResponseUtils.onCallFailed(homeActivity);
                }
            }, ResponseCodeHttpConstant.SUCCESS));

            etSearchBar.setText("");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        location = new Gson().fromJson((String) BundleUtils.getValue(getArguments(), BundleConstant.BUNDLE_LOCATION), Location.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentStockListingBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_stock_listing, container, false);
        View root = fragmentStockListingBinding.getRoot();
        fragmentStockListingBinding.setLocation(location);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (location.getLabel() != null) {
            setToolbarTitle(location.getLabel());
        }
    }

    @Override
    protected void afterActivityCreated() {
        initAdapter();
    }

    private void initAdapter() {
        exemplaryLocationListingAdapter = new ExemplaryLocationListingAdapter(homeActivity, swipeRefresh, location.getId(), new ExemplaryLocationListingHolder.ExemplaryLocationListingListener() {
            @Override
            public void onClick(Exemplary exemplary) {
                PopupExemplaryDetail popupExemplaryDetail = PopupExemplaryDetail.getInstance(homeActivity, exemplary);
                popupExemplaryDetail.show();
            }

            @Override
            public void onMove(Exemplary exemplary) {
                MoveFragment moveFragment = new MoveFragment();

                Bundle b = new Bundle();
                b.putString(BundleConstant.BUNDLE_EXEMPLARY, new Gson().toJson(exemplary));
                moveFragment.setArguments(b);

                homeActivity.pushFragment(moveFragment);
            }
        });

        rvStocks.setLayoutManager(new LinearLayoutManager(homeActivity));
        rvStocks.addItemDecoration(new MarginItemDecoration(8));
        rvStocks.setAdapter(exemplaryLocationListingAdapter);
    }

    @OnTextChanged(R.id.et_search_bar)
    public void searchBarTextChanged() {
        exemplaryLocationListingAdapter.refreshKeyword(etSearchBar.getText().toString());
    }
}













