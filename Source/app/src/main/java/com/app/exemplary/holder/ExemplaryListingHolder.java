package com.app.exemplary.holder;

import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.R;
import com.app.databinding.ObjectExemplaryListingBinding;
import com.app.retrofit.model.object.Exemplary;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExemplaryListingHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_move)
    ImageView ivMove;
    @BindView(R.id.iv_lend)
    ImageView ivLend;

    private final ObjectExemplaryListingBinding objectExemplaryListingBinding;
    private final ExemplaryListingListener exemplaryListingListener;

    public ExemplaryListingHolder(ObjectExemplaryListingBinding objectExemplaryListingBinding, ExemplaryListingListener exemplaryListingListener) {
        super(objectExemplaryListingBinding.getRoot());
        ButterKnife.bind(this, objectExemplaryListingBinding.getRoot());

        this.objectExemplaryListingBinding = objectExemplaryListingBinding;
        this.exemplaryListingListener = exemplaryListingListener;
    }

    public void bind(Exemplary exemplary, boolean moveMode, boolean lendMode) {
        objectExemplaryListingBinding.setExemplary(exemplary);
        objectExemplaryListingBinding.setMove(moveMode);
        objectExemplaryListingBinding.setLend(lendMode);

        ivMove.setOnClickListener(v -> exemplaryListingListener.onMoveRequest(exemplary));
        ivLend.setOnClickListener(v -> exemplaryListingListener.onLend(exemplary));
        itemView.setOnClickListener(v -> exemplaryListingListener.onClick(exemplary));
    }

    public interface ExemplaryListingListener {
        void onClick(Exemplary exemplary);

        void onLend(Exemplary exemplary);

        void onMoveRequest(Exemplary exemplary);
    }
}













