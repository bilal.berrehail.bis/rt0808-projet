package com.app.exemplary.holder;

import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.R;
import com.app.databinding.ObjectStockListingBinding;
import com.app.retrofit.model.object.Exemplary;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExemplaryLocationListingHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_move)
    ImageView ivMove;

    private final ObjectStockListingBinding objectStockListingBinding;
    private final ExemplaryLocationListingListener exemplaryLocationListingListener;

    public ExemplaryLocationListingHolder(ObjectStockListingBinding objectStockListingBinding, ExemplaryLocationListingListener exemplaryLocationListingListener) {
        super(objectStockListingBinding.getRoot());
        ButterKnife.bind(this, itemView);

        this.objectStockListingBinding = objectStockListingBinding;
        this.exemplaryLocationListingListener = exemplaryLocationListingListener;
    }

    public void bind(Exemplary exemplary) {
        objectStockListingBinding.setExemplary(exemplary);

        itemView.setOnClickListener(v -> exemplaryLocationListingListener.onClick(exemplary));
        ivMove.setOnClickListener(v -> exemplaryLocationListingListener.onMove(exemplary));
    }

    public interface ExemplaryLocationListingListener {
        void onClick(Exemplary exemplary);
        void onMove(Exemplary exemplary);
    }
}





















