package com.app.exemplary.popup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.R;
import com.app.dialog.helper.DialogHelper;
import com.app.home.HomeActivity;
import com.app.location.adapter.LocationListingAdapter;
import com.app.location.holder.LocationListingHolder;
import com.app.model.constant.BundleConstant;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.model.fields.ExemplaryFields;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Exemplary;
import com.app.retrofit.model.object.Location;
import com.app.retrofit.model.response.ExemplaryResponse;
import com.app.utils.CallbackEntity;
import com.app.utils.ViewUtils;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class PopupCreateExemplary extends Dialog {
    @BindView(R.id.et_exemplary_name)
    EditText etExemplaryName;
    @BindView(R.id.iv_edit_location_arrival)
    ImageView ivEditLocationArrival;
    @BindView(R.id.ll_select_location)
    LinearLayout llSelectLocation;
    @BindView(R.id.tv_location_arrival_name)
    TextView tvLocationArrivalName;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_create)
    Button btnCreate;

    private final Context context;
    private final String item_id;

    private final CreateExemplaryListener createExemplaryListener;

    private PopupWindow popupSelectLocation;
    private Location locationArrival;

    @SuppressLint("StaticFieldLeak")
    private static PopupCreateExemplary popupCreateExemplary;

    private PopupCreateExemplary(Context context, String item_id, CreateExemplaryListener createExemplaryListener) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
        this.item_id = item_id;
        this.createExemplaryListener = createExemplaryListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_create_exemplary, null);

        ButterKnife.bind(this, view);

        setContentView(view);
        initView();

        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                onViewCompleteLoaded();
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    public static PopupCreateExemplary getInstance(Context context, String item_id, CreateExemplaryListener createExemplaryListener) {
        if (popupCreateExemplary == null) {
            popupCreateExemplary = new PopupCreateExemplary(context, item_id, createExemplaryListener);
        }
        return popupCreateExemplary;
    }

    @Override
    public void dismiss() {
        Bundle bundleUpdate = new Bundle();
        bundleUpdate.putBoolean(BundleConstant.BUNDLE_UPDATE, true);
        ((HomeActivity) context).remoteCallCurrentFragment(bundleUpdate);

        popupCreateExemplary = null;
        super.dismiss();
    }

    private void onViewCompleteLoaded() {
        LocationListingAdapter locationListingAdapter = new LocationListingAdapter(context, null, false, false, false, new LocationListingHolder.LocationListingListener() {
            @Override
            public void onClick(Location location) {
                locationArrival = location;
                if (location.getLabel() != null) {
                    tvLocationArrivalName.setText(location.getLabel());
                }
                if (popupSelectLocation != null) {
                    popupSelectLocation.dismiss();
                }
            }

            @Override
            public void onDelete(Location location) {
            }

            @Override
            public void onConsultationExemplarys(Location location) {
            }
        });

        popupSelectLocation = ViewUtils.createPopupMenu(context, llSelectLocation, locationListingAdapter, new LinearLayoutManager(context));
    }

    private void initView() {
        ivEditLocationArrival.setOnClickListener(v -> {
            if (popupSelectLocation != null) {
                popupSelectLocation.showAsDropDown(llSelectLocation);
                ViewUtils.hideKeyboard(getWindow());
            }
        });

        btnCancel.setOnClickListener(v -> dismiss());
        btnCreate.setOnClickListener(v -> {
            if (locationArrival == null) {
                ViewUtils.toast(context, R.string.select_location, true);
                return;
            }

            Exemplary newExemplary = new Exemplary();
            newExemplary.setLabel(etExemplaryName.getText().toString());
            newExemplary.setLocation(locationArrival);

            DialogHelper.getInstance().showLoading(context);

            HashMap<String, Object> mapExemplary = ExemplaryFields.serializeForCreation(newExemplary);

            Call<ExemplaryResponse> call = RetrofitInstance.getAPIService().createExemplary(item_id, locationArrival.getId(), mapExemplary);
            call.enqueue(new CallbackEntity<>(ExemplaryResponse.class, new CallbackEntity.CallbackEntityResponse<ExemplaryResponse>() {
                @Override
                public void onSuccess(ExemplaryResponse exemplaryResponse) {
                    DialogHelper.getInstance().dismissDialog();

                    Exemplary exemplary = exemplaryResponse.getEntity();

                    ViewUtils.toast(context, R.string.exemplary_created, false);
                    createExemplaryListener.onSuccess(exemplary);
                    dismiss();
                }

                @Override
                public void onFailure() {
                    DialogHelper.getInstance().dismissDialog();
                    ViewUtils.toast(context, R.string.error_create_exemplary, true);
                    dismiss();
                }
            }, ResponseCodeHttpConstant.CREATE_SUCCESS));
        });
    }

    public interface CreateExemplaryListener {
        void onSuccess(Exemplary exemplary);
    }
}
