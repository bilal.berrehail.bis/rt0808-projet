package com.app.exemplary.popup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.databinding.DataBindingUtil;

import com.app.R;
import com.app.databinding.PopupExemplaryDetailBinding;
import com.app.dialog.PopupConfirmCancel;
import com.app.dialog.helper.DialogHelper;
import com.app.home.HomeActivity;
import com.app.lend.fragment.LendFragment;
import com.app.lend.popup.PopupLoanRecovery;
import com.app.model.constant.BundleConstant;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.model.fields.ExemplaryFields;
import com.app.move.MoveFragment;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Exemplary;
import com.app.retrofit.model.response.DeleteExemplaryResponse;
import com.app.retrofit.model.response.UpdateExemplaryResponse;
import com.app.utils.CallbackEntity;
import com.app.utils.ViewUtils;
import com.app.view.PopupBitmap;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class PopupExemplaryDetail extends Dialog {
    @BindView(R.id.iv_validate)
    ImageView ivValidate;
    @BindView(R.id.iv_delete)
    ImageView ivDelete;
    @BindView(R.id.iv_move)
    ImageView ivMove;
    @BindView(R.id.iv_lend)
    ImageView ivLend;
    @BindView(R.id.iv_recovery)
    ImageView ivRecovery;
    @BindView(R.id.btn_build_qr_code)
    Button btnBuildQrCode;
    @BindView(R.id.btn_back)
    Button btnBack;

    private final Context context;
    private Exemplary exemplary;

    @SuppressLint("StaticFieldLeak")
    private static PopupExemplaryDetail popupExemplaryDetail;

    private PopupExemplaryDetail(Context context, Exemplary exemplary) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
        try {
            this.exemplary = (Exemplary) exemplary.clone();
        } catch (CloneNotSupportedException ex) {
            dismiss();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);

        PopupExemplaryDetailBinding popupExemplaryDetailBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.popup_exemplary_detail, null, false);
        popupExemplaryDetailBinding.setExemplary(exemplary);

        View view = popupExemplaryDetailBinding.getRoot();

        setContentView(view);
        ButterKnife.bind(this, view);

        initView();
    }

    @Override
    public void dismiss() {
        popupExemplaryDetail = null;

        Bundle bundleUpdate = new Bundle();
        bundleUpdate.putBoolean(BundleConstant.BUNDLE_UPDATE, true);
        ((HomeActivity) context).remoteCallCurrentFragment(bundleUpdate);

        super.dismiss();
    }

    public static PopupExemplaryDetail getInstance(Context context, Exemplary exemplary) {
        if (popupExemplaryDetail == null) {
            popupExemplaryDetail = new PopupExemplaryDetail(context, exemplary);
        }
        return popupExemplaryDetail;
    }

    private void initView() {
        ivValidate.setOnClickListener(v -> {
            if (exemplary.getLendInformation() != null) {
                PopupConfirmCancel popupConfirmCancel = PopupConfirmCancel.getInstance(context, R.string.confirm_update_exemplary_lent, this::updateExemplary);
                popupConfirmCancel.show();
            } else {
                updateExemplary();
            }
        });

        ivDelete.setOnClickListener(v -> {
            PopupConfirmCancel popupConfirmCancel = PopupConfirmCancel.getInstance(context, R.string.confirm_delete_exemplary, () -> {
                DialogHelper.getInstance().showLoading(context);

                Call<DeleteExemplaryResponse> call = RetrofitInstance.getAPIService().deleteExemplary(exemplary.getId());
                call.enqueue(new CallbackEntity<>(DeleteExemplaryResponse.class, new CallbackEntity.CallbackEntityResponse<DeleteExemplaryResponse>() {
                    @Override
                    public void onSuccess(DeleteExemplaryResponse deleteExemplaryResponse) {
                        DialogHelper.getInstance().dismissDialog();
                        ViewUtils.toast(context, R.string.exemplary_deleted, false);
                        dismiss();
                    }

                    @Override
                    public void onFailure() {
                        DialogHelper.getInstance().dismissDialog();
                        ViewUtils.toast(context, R.string.error_deleting_exemplary, true);
                    }
                }, ResponseCodeHttpConstant.SUCCESS));
            });
            popupConfirmCancel.show();
        });

        ivMove.setOnClickListener(v -> {
            MoveFragment moveFragment = new MoveFragment();

            Bundle b = new Bundle();
            b.putString(BundleConstant.BUNDLE_EXEMPLARY, new Gson().toJson(exemplary));
            moveFragment.setArguments(b);

            ((HomeActivity) context).pushFragment(moveFragment);
            dismiss();
        });

        ivLend.setOnClickListener(v -> {
            LendFragment lendFragment = new LendFragment();

            Bundle b = new Bundle();
            b.putString(BundleConstant.BUNDLE_EXEMPLARY, new Gson().toJson(exemplary));
            lendFragment.setArguments(b);

            ((HomeActivity) context).pushFragment(lendFragment);
            dismiss();
        });

        ivRecovery.setOnClickListener(v -> {
            PopupLoanRecovery popupLoanRecovery = PopupLoanRecovery.getInstance(context, exemplary);
            popupLoanRecovery.show();
        });

        btnBuildQrCode.setOnClickListener(v -> {
            HashMap<String, String> mapExemplary = new HashMap<>();
            mapExemplary.put("exemplary_id", exemplary.getId());
            Bitmap qrCodeBmp = ViewUtils.generateQrCode(new Gson().toJson(mapExemplary), ViewUtils.getString(exemplary.getLabel()));
            PopupBitmap popupBitmap = PopupBitmap.getInstance(context, qrCodeBmp);
            popupBitmap.show();
        });

        btnBack.setOnClickListener(v -> dismiss());
    }

    private void updateExemplary() {
        DialogHelper.getInstance().showLoading(context);

        HashMap<String, Object> mapExemplary = ExemplaryFields.serializeForUpdating(exemplary);

        Call<UpdateExemplaryResponse> call = RetrofitInstance.getAPIService().updateExemplaryById(exemplary.getId(), mapExemplary);
        call.enqueue(new CallbackEntity<>(UpdateExemplaryResponse.class, new CallbackEntity.CallbackEntityResponse<UpdateExemplaryResponse>() {
            @Override
            public void onSuccess(UpdateExemplaryResponse updateExemplaryResponse) {
                DialogHelper.getInstance().dismissDialog();
                ViewUtils.toast(context, R.string.update_exemplary_success, false);
                dismiss();
            }

            @Override
            public void onFailure() {
                DialogHelper.getInstance().dismissDialog();
                ViewUtils.toast(context, R.string.update_exemplary_failed, true);
            }
        }, ResponseCodeHttpConstant.SUCCESS));
    }
}
