package com.app.home;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.app.R;
import com.app.base.BaseFragment;
import com.app.library.fragment.LibraryListingFragment;
import com.app.login.LoginActivity;
import com.app.model.UserInstance;
import com.app.model.constant.IntentConstant;
import com.app.utils.ViewUtils;

import java.util.Stack;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {
    @BindView(R.id.custom_toolbar)
    Toolbar customToolbar;

    private FragmentManager fragmentManager;
    private Stack<BaseFragment> backStackBF;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        ButterKnife.bind(this);
        setSupportActionBar(customToolbar);

        fragmentManager = getSupportFragmentManager();
        backStackBF = new Stack<>();

        customToolbar.setNavigationOnClickListener(v -> onBackPressed());

        LibraryListingFragment libraryListingFragment = new LibraryListingFragment();
        pushFragment(libraryListingFragment);
    }

    public void setToolbarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    public void setToolbarTitle(int title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    private void clearToolbarMenu() {
        customToolbar.getMenu().clear();
        customToolbar.setOnMenuItemClickListener(null);
    }

    public void inflateToolbarMenu(int id, Toolbar.OnMenuItemClickListener listener) {
        clearToolbarMenu();
        getMenuInflater().inflate(id, customToolbar.getMenu());
        customToolbar.setOnMenuItemClickListener(listener);
    }

    public void removeItemToolbarMenu(int item_id) {
        if (customToolbar.getMenu().findItem(item_id) != null) {
            customToolbar.getMenu().removeItem(item_id);
        }
    }

    public void backToLogin() {
        UserInstance.initializeUser(null);
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.putExtra(IntentConstant.INTENT_DELETE_CREDENTIALS, true);
        startActivity(intent);
        finish();
    }

    public void onBackPressed() {
        if (backStackBF.size() > 0) {
            if (backStackBF.get(backStackBF.size() - 1).onBackPressed()) {
                popBackStack();
            }
        }
    }

    public void popBackStack() {
        if (!backStackBF.empty()) {
            ViewUtils.hideKeyboard(this);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentManager.popBackStack();
            backStackBF.pop();

            fragmentTransaction.commit();

            refreshNavigationIcon();
        }
    }

    private void refreshNavigationIcon() {
        if (!backStackBF.empty()) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(backStackBF.size() > 1);
                getSupportActionBar().setHomeButtonEnabled(backStackBF.size() > 1);
            }
        }
    }

    public void pushFragment(BaseFragment baseFragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);

        if (backStackBF.empty()) {
            fragmentTransaction.add(R.id.fl_main, baseFragment, baseFragment.getClass().getName()).addToBackStack(baseFragment.getClass().getName()).commit();
        } else {
            BaseFragment currentFragment = backStackBF.lastElement();
            fragmentTransaction.add(R.id.fl_main, baseFragment, baseFragment.getClass().getName()).hide(currentFragment).addToBackStack(baseFragment.getClass().getName()).commit();
        }

        backStackBF.add(baseFragment);

        refreshNavigationIcon();
    }

    public void remoteCallCurrentFragment(Bundle bundle) {
        if (!backStackBF.empty()) {
            backStackBF.get(backStackBF.size() - 1).remoteCall(bundle);
        }
    }

    public void remoteCallPreviousFragment(Bundle bundle) {
        if (backStackBF.size() > 1) {
            backStackBF.get(backStackBF.size() - 2).remoteCall(bundle);
        }
    }

    public void remoteCallActivityResult(Intent intent, int requestCode, BaseFragment.ActivityResultListener activityResultListener) {
        if (!backStackBF.empty()) {
            backStackBF.get(backStackBF.size() - 1).callActivityForResult(intent, requestCode, activityResultListener);
        }
    }

    public BaseFragment getCurrentFragment() {
        if (!backStackBF.empty()) {
            return backStackBF.get(backStackBF.size() - 1);
        }
        return null;
    }
}