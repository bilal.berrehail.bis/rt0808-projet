package com.app.home;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.GridLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.app.R;
import com.app.base.BaseFragment;
import com.app.category.popup.PopupCategoryListing;
import com.app.exemplary.popup.PopupExemplaryDetail;
import com.app.item.fragment.ItemListingFragment;
import com.app.item.popup.PopupCreateModifyItem;
import com.app.lend.fragment.LendFragment;
import com.app.library.popup.PopupLibraryDetail;
import com.app.location.fragment.LocationListingFragment;
import com.app.model.UserInstance;
import com.app.model.constant.ScanConstant;
import com.app.move.MoveFragment;
import com.app.retrofit.model.object.Exemplary;
import com.app.scanner.PopupScan;
import com.app.utils.DataScanUtils;
import com.app.utils.ViewUtils;
import com.google.zxing.Result;

import butterknife.BindView;

public class HomeFragment extends BaseFragment implements Toolbar.OnMenuItemClickListener {
    @BindView(R.id.bg_movible)
    ImageView bgMovible;
    @BindView(R.id.iv_home)
    ImageView ivWelcome;
    @BindView(R.id.gl_menus)
    GridLayout glMenus;
    @BindView(R.id.cv_consultation_items)
    CardView cvConsultationItems;
    @BindView(R.id.cv_consultation_categories)
    CardView cvConsultationCategories;
    @BindView(R.id.cv_consultation_locations)
    CardView cvConsultationLocations;
    @BindView(R.id.cv_move)
    CardView cvMove;
    @BindView(R.id.cv_lend)
    CardView cvLend;
    @BindView(R.id.cv_scan)
    CardView cvScan;

    private PopupScan popupScan;

    @Override
    protected int getLayout() {
        return R.layout.fragment_home;
    }

    @Override
    protected int getTitle() {
        return R.string.title_home;
    }

    @Override
    protected void afterActivityCreated() {
        cvConsultationItems.setOnClickListener(v -> {
            ItemListingFragment itemListingFragment = new ItemListingFragment();

            homeActivity.pushFragment(itemListingFragment);
        });
        cvConsultationCategories.setOnClickListener(v -> openCategoryListing());
        cvConsultationLocations.setOnClickListener(v -> {
            LocationListingFragment locationListingFragment = new LocationListingFragment();

            homeActivity.pushFragment(locationListingFragment);
        });
        cvMove.setOnClickListener(v -> {
            MoveFragment moveFragment = new MoveFragment();
            homeActivity.pushFragment(moveFragment);
        });
        cvLend.setOnClickListener(v -> {
            LendFragment lendFragment = new LendFragment();
            homeActivity.pushFragment(lendFragment);
        });
        cvScan.setOnClickListener(v -> {
            popupScan = PopupScan.getInstance(homeActivity, result -> {
                popupScan.dismiss();
                processScanResult(result);
            });
            popupScan.show();
        });
    }

    @Override
    protected void onViewCompleteLoaded() {
        super.onViewCompleteLoaded();
        loadAnimation();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        homeActivity.inflateToolbarMenu(R.menu.menu_home, this);
    }

    private void loadAnimation() {
        Animation fromBottomFade = AnimationUtils.loadAnimation(homeActivity, R.anim.from_bottom_fade);
        Animation shake = AnimationUtils.loadAnimation(homeActivity, R.anim.shake);

        bgMovible.animate().y(ivWelcome.getY() - bgMovible.getHeight() + ivWelcome.getHeight() + TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                8,
                getResources().getDisplayMetrics()
        )).setDuration(600);
        glMenus.animate().alpha(1.0f).setDuration(800);

        fromBottomFade.setStartOffset(1200);
        glMenus.startAnimation(fromBottomFade);

        ivWelcome.startAnimation(fromBottomFade);
        ivWelcome.setOnClickListener(v -> ivWelcome.startAnimation(shake));
    }

    private void processScanResult(Result scanResult) {
        if (scanResult.getBarcodeFormat() == null) return;

        String result = scanResult.getText();

        if (scanResult.getBarcodeFormat().toString().equals(ScanConstant.ISBN_FORMAT)) {
            PopupCreateModifyItem popupCreateModifyItem = PopupCreateModifyItem.getInstance(homeActivity, result);
            popupCreateModifyItem.show();
        } else if (scanResult.getBarcodeFormat().toString().equals(ScanConstant.EXEMPLARY_FORMAT)) {
            DataScanUtils.buildExemplary(scanResult.getText(), new DataScanUtils.DataScanUtilsListener() {
                @Override
                public void onBuildExemplary(Exemplary exemplary) {
                    PopupExemplaryDetail popupExemplaryDetail = PopupExemplaryDetail.getInstance(homeActivity, exemplary);
                    popupExemplaryDetail.show();
                }

                @Override
                public void onBuildFailed() {
                    ViewUtils.toast(homeActivity, R.string.no_exemplary_found, true);
                }
            });
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_signout:
                homeActivity.backToLogin();
                return true;
            case R.id.action_info:
                if (UserInstance.getUser() != null && UserInstance.getUser().getLibrary() != null) {
                    PopupLibraryDetail popupLibraryDetail = PopupLibraryDetail.getInstance(homeActivity, UserInstance.getUser().getLibrary());
                    popupLibraryDetail.show();
                } else {
                    ViewUtils.toast(homeActivity, R.string.error_occured, true);
                }
                return true;
        }
        return false;
    }

    private void openCategoryListing() {
        PopupCategoryListing popupCategoryListing = PopupCategoryListing.getInstance(homeActivity, true, true);
        popupCategoryListing.show();
    }
}
