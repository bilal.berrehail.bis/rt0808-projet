package com.app.item.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.R;
import com.app.databinding.ObjectItemListingBinding;
import com.app.item.holder.ItemListingHolder;
import com.app.retrofit.model.object.Item;
import com.app.retrofit.model.response.ListItemResponse;
import com.app.retrofit.utils.EntityUtils;
import com.app.view.PaginationAdapter;

import io.reactivex.Observable;

import static com.app.retrofit.utils.EntityUtils.LIMIT_GET_ITEMS_PER_PAGE;

public class ItemListingAdapter extends PaginationAdapter<ListItemResponse, ItemListingHolder> {
    private final ItemListingHolder.ItemListingHolderListener itemListingHolderListener;

    public ItemListingAdapter(Context context, SwipeRefreshLayout swipeRefreshLayout, ItemListingHolder.ItemListingHolderListener itemListingHolderListener) {
        super(context, swipeRefreshLayout);
        this.itemListingHolderListener = itemListingHolderListener;

        start(this);
    }

    @NonNull
    @Override
    public ItemListingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ObjectItemListingBinding objectItemListingBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.object_item_listing, parent, false);
        return new ItemListingHolder(objectItemListingBinding, itemListingHolderListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemListingHolder holder, int position) {
        holder.bind((Item) entities.get(position));
    }

    @Override
    protected Observable<ListItemResponse> getObservable() {
        return EntityUtils.getItemsRx(currentPage, LIMIT_GET_ITEMS_PER_PAGE, keyword, null, null);
    }
}
