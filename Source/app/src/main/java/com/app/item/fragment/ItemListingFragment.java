package com.app.item.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.R;
import com.app.base.BaseFragment;
import com.app.item.adapter.ItemListingAdapter;
import com.app.item.holder.ItemListingHolder;
import com.app.item.popup.PopupCreateModifyItem;
import com.app.item.popup.PopupItemDetail;
import com.app.model.constant.BundleConstant;
import com.app.model.decoration.MarginItemDecoration;
import com.app.retrofit.model.object.Item;
import com.app.scanner.PopupScan;
import com.app.utils.BundleUtils;
import com.google.zxing.Result;

import butterknife.BindView;
import butterknife.OnTextChanged;

public class ItemListingFragment extends BaseFragment implements ItemListingHolder.ItemListingHolderListener, Toolbar.OnMenuItemClickListener, PopupScan.PopupScanListener {
    @BindView(R.id.et_search_bar)
    EditText etSearchBar;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.rv_items)
    RecyclerView rvItems;

    private ItemListingAdapter itemListingAdapter;

    private RemoteUseListener remoteUseListener;

    private PopupScan popupScan;

    private boolean addMode = true;

    @Override
    protected int getLayout() {
        return R.layout.fragment_item_listing;
    }

    @Override
    protected int getTitle() {
        return R.string.consultation_items;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        rvItems.setAdapter(null);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof RemoteUseListener) {
            remoteUseListener = (RemoteUseListener) getParentFragment();
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            etSearchBar.setText("");
        }
    }

    @Override
    public void remoteCall(Bundle bundle) {
        if (BundleUtils.getValue(bundle, BundleConstant.BUNDLE_UPDATE) != null) {
            etSearchBar.setText("");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        Object addModeValue = BundleUtils.getValue(getArguments(), BundleConstant.BUNDLE_ADD_MODE);
        if (addModeValue != null) {
            addMode = (Boolean) addModeValue;
        }
    }

    @Override
    protected void afterActivityCreated() {
        itemListingAdapter = new ItemListingAdapter(homeActivity, swipeRefresh, this);
        rvItems.setLayoutManager(new LinearLayoutManager(homeActivity));
        rvItems.addItemDecoration(new MarginItemDecoration(8));
        rvItems.setAdapter(itemListingAdapter);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        homeActivity.inflateToolbarMenu(R.menu.menu_item_listing, this);
        if (!addMode) {
            homeActivity.removeItemToolbarMenu(R.id.action_add);
        }
    }

    @Override
    public void onItemClick(Item item) {
        if (remoteUseListener != null) {
            remoteUseListener.onItemClick(item);
        } else {
            PopupItemDetail.getInstance(homeActivity, item).show();
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_scan:
                popupScan = PopupScan.getInstance(homeActivity, this);
                popupScan.show();
                return true;
            case R.id.action_add:
                PopupCreateModifyItem.getInstance(homeActivity).show();
                return true;
        }
        return false;
    }

    @OnTextChanged(R.id.et_search_bar)
    public void searchBarTextChanged() {
        itemListingAdapter.refreshKeyword(etSearchBar.getText().toString());
    }

    @Override
    public void onResult(Result result) {
        if (popupScan != null) {
            popupScan.dismiss();
        }
        PopupCreateModifyItem popupCreateItem = PopupCreateModifyItem.getInstance(homeActivity, result.getText());
        popupCreateItem.show();
    }

    public interface RemoteUseListener {
        void onItemClick(Item item);
    }
}
