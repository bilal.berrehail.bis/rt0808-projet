package com.app.item.holder;

import androidx.recyclerview.widget.RecyclerView;

import com.app.databinding.ObjectItemListingBinding;
import com.app.retrofit.model.object.Item;

public class ItemListingHolder extends RecyclerView.ViewHolder {
    private final ItemListingHolderListener itemListingHolderListener;
    private final ObjectItemListingBinding objectItemListingBinding;

    public ItemListingHolder(ObjectItemListingBinding objectItemListingBinding, ItemListingHolderListener itemListingHolderListener) {
        super(objectItemListingBinding.getRoot());

        this.objectItemListingBinding = objectItemListingBinding;
        this.itemListingHolderListener = itemListingHolderListener;
    }

    public void bind(Item item) {
        objectItemListingBinding.setItem(item);
        itemView.setOnClickListener(v -> itemListingHolderListener.onItemClick(item));
    }

    public interface ItemListingHolderListener {
        void onItemClick(Item item);
    }
}
