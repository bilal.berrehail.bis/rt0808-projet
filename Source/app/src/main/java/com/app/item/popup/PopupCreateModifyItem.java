package com.app.item.popup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.R;
import com.app.category.adapter.CategoryListingAdapter;
import com.app.category.holder.CategoryListingHolder;
import com.app.databinding.PopupCreateModifyItemBinding;
import com.app.dialog.PopupConfirmCancel;
import com.app.dialog.helper.DialogHelper;
import com.app.home.HomeActivity;
import com.app.model.constant.BundleConstant;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.model.fields.ItemFields;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Category;
import com.app.retrofit.model.object.Item;
import com.app.retrofit.model.object.volume.Volume;
import com.app.retrofit.model.response.DeleteItemResponse;
import com.app.retrofit.model.response.ItemResponse;
import com.app.retrofit.model.response.ListItemResponse;
import com.app.retrofit.model.response.ListVolumeResponse;
import com.app.retrofit.model.response.UpdateItemResponse;
import com.app.retrofit.model.response.UploadImageResponse;
import com.app.retrofit.utils.ResponseUtils;
import com.app.utils.CallbackEntity;
import com.app.utils.CounterMutex;
import com.app.utils.StorageUtils;
import com.app.utils.ViewUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class PopupCreateModifyItem extends Dialog {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_item_image)
    ImageView ivItemImage;
    @BindView(R.id.iv_item_delete)
    ImageView ivItemDelete;
    @BindView(R.id.cl_header_card)
    ConstraintLayout clHeaderCard;
    @BindView(R.id.iv_edit_img)
    ImageView ivEditImg;
    @BindView(R.id.iv_delete_category)
    ImageView ivDeleteCategory;
    @BindView(R.id.tv_category_name)
    TextView tvCategoryName;
    @BindView(R.id.iv_edit_category)
    ImageView ivEditCategory;
    @BindView(R.id.btn_back)
    Button btnBack;
    @BindView(R.id.btn_validate)
    Button btnValidate;

    private final Context context;

    private PopupWindow popupSelectCategory;

    @SuppressLint("StaticFieldLeak")
    private static PopupCreateModifyItem popupCreateModifyItem;

    private Item item;
    private boolean isModifyMode;

    private File imgFileToUpload;

    private final PopupCreateModifyItemBinding popupCreateModifyItemBinding;

    private PopupCreateModifyItem(Context context, Item item) {
        this(context);
        setModifyMode(item);
    }

    private PopupCreateModifyItem(Context context, String isbn) {
        this(context);
        Call<ListItemResponse> call = RetrofitInstance.getAPIService().getItems(1, 0, null, ItemFields.isbn, isbn);
        call.enqueue(new CallbackEntity<>(ListItemResponse.class, new CallbackEntity.CallbackEntityResponse<ListItemResponse>() {
            @Override
            public void onSuccess(ListItemResponse listItemResponse) {
                ArrayList<Item> items = listItemResponse.getEntities();

                if (items == null || items.size() == 0) {
                    fetchVolumeByISBN(isbn);
                    return;
                }

                setModifyMode(items.get(0));
            }

            @Override
            public void onFailure() {
                fetchVolumeByISBN(isbn);
            }
        }, ResponseCodeHttpConstant.SUCCESS));
    }

    private PopupCreateModifyItem(Context context) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
        this.item = new Item();

        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);

        popupCreateModifyItemBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.popup_create_modify_item, null, false);

        popupCreateModifyItemBinding.setItem(item);

        View view = popupCreateModifyItemBinding.getRoot();

        setContentView(view);
        ButterKnife.bind(this, view);

        initView();

        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                initAdapter();
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private void setModifyMode(Item item) {
        this.item = item;
        popupCreateModifyItemBinding.setItem(item);

        isModifyMode = true;

        tvTitle.setText(R.string.modify_item);
        btnValidate.setText(R.string.modify);
        ivItemDelete.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismiss() {
        Bundle bundleUpdate = new Bundle();
        bundleUpdate.putBoolean(BundleConstant.BUNDLE_UPDATE, true);
        ((HomeActivity) context).remoteCallCurrentFragment(bundleUpdate);

        popupCreateModifyItem = null;
        super.dismiss();
    }

    public static PopupCreateModifyItem getInstance(Context context) {
        if (popupCreateModifyItem == null) {
            popupCreateModifyItem = new PopupCreateModifyItem(context);
        }
        return popupCreateModifyItem;
    }

    public static PopupCreateModifyItem getInstance(Context context, String isbn) {
        if (popupCreateModifyItem == null) {
            popupCreateModifyItem = new PopupCreateModifyItem(context, isbn);
        }
        return popupCreateModifyItem;
    }

    public static PopupCreateModifyItem getInstance(Context context, Item item) {
        if (popupCreateModifyItem == null) {
            popupCreateModifyItem = new PopupCreateModifyItem(context, item);
        }
        return popupCreateModifyItem;
    }

    private void initView() {
        ivItemDelete.setOnClickListener(v -> {
            PopupConfirmCancel popupConfirmCancel = PopupConfirmCancel.getInstance(context, R.string.confirm_delete_item, () -> {
                DialogHelper.getInstance().showLoading(context);
                deleteItem();
            });
            popupConfirmCancel.show();
        });

        ivDeleteCategory.setOnClickListener(v -> item.setCategory(null));

        ivEditImg.setOnClickListener(v -> {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            ((HomeActivity) context).remoteCallActivityResult(gallery, 100, intent -> {

                if (intent.getData() != null && intent.getData().getPath() != null) {
                    imgFileToUpload = StorageUtils.moveFileToLocalDirectory(context, intent.getData());

                    if (imgFileToUpload == null) {
                        ViewUtils.toast(context, R.string.error_load_image, true);
                        return;
                    }

                    ViewUtils.loadImage(ivItemImage, intent.getData());
                }

            });
        });

        ivEditCategory.setOnClickListener(v -> {
            if (popupSelectCategory != null) {
                popupSelectCategory.showAsDropDown(clHeaderCard);
                ViewUtils.hideKeyboard(getWindow());
            }
        });

        btnBack.setOnClickListener(v -> dismiss());
        btnValidate.setOnClickListener(v -> onValidate());
    }

    private void deleteItem() {
        Call<DeleteItemResponse> call = RetrofitInstance.getAPIService().deleteItemById(item.getId());
        call.enqueue(new CallbackEntity<>(DeleteItemResponse.class, new CallbackEntity.CallbackEntityResponse<DeleteItemResponse>() {
            @Override
            public void onSuccess(DeleteItemResponse deleteItemResponse) {
                DialogHelper.getInstance().dismissDialog();
                if (deleteItemResponse.isSuccessful()) {
                    ViewUtils.toast(context, R.string.item_deleted, false);
                    dismiss();
                } else {
                    if (deleteItemResponse.getTotal_exemplary_related() != null) {
                        ViewUtils.toast(context, context.getString(R.string.error_deleting_item, deleteItemResponse.getTotal_exemplary_related()), true);
                    }
                }
            }

            @Override
            public void onFailure() {
                DialogHelper.getInstance().dismissDialog();
                ResponseUtils.onCallFailed(context);
            }
        }, ResponseCodeHttpConstant.SUCCESS, ResponseCodeHttpConstant.METHOD_NOT_ALLOWED));
    }

    private void initAdapter() {
        CategoryListingAdapter categoryListingAdapter = new CategoryListingAdapter(context, null, false, new CategoryListingHolder.CategoryListingHolderListener() {
            @Override
            public void onCategoryClick(Category category) {
                item.setCategory(category);
                if (popupSelectCategory != null) {
                    popupSelectCategory.dismiss();
                }
            }

            @Override
            public void onCategoryDelete(Category category) {

            }
        });

        popupSelectCategory = ViewUtils.createPopupMenu(context, clHeaderCard, categoryListingAdapter, new LinearLayoutManager(context));
    }

    private void fetchVolumeByISBN(String isbn) {
        DialogHelper.getInstance().showLoading(context);
        item.setIsbn(isbn);

        Call<ListVolumeResponse> call = RetrofitInstance.getAPIServiceWithoutToken().getVolumeByISNB("https://www.googleapis.com/books/v1/volumes?q=isbn:" + isbn);
        call.enqueue(new CallbackEntity<>(ListVolumeResponse.class, new CallbackEntity.CallbackEntityResponse<ListVolumeResponse>() {
            @Override
            public void onSuccess(ListVolumeResponse listVolumeResponse) {
                ArrayList<Volume> volumes = listVolumeResponse.getEntities();

                if (volumes != null && volumes.size() > 0) {
                    Volume volume = volumes.get(0);

                    if (volume.getVolumeInfo() == null) {
                        ViewUtils.toast(context, R.string.isbn_not_found, true);
                        return;
                    }

                    if (volume.getVolumeInfo().getTitle() != null) {
                        item.setLabel(volume.getVolumeInfo().getTitle());
                    }

                    if (volume.getVolumeInfo().getAuthors() != null && volume.getVolumeInfo().getAuthors().length > 0) {
                        item.setAuthor(volume.getVolumeInfo().getAuthors()[0]);
                    }

                    if (volume.getVolumeInfo().getPublisher() != null) {
                        item.setEditor(volume.getVolumeInfo().getPublisher());
                    }

                    if (volume.getVolumeInfo().getPublishedDate() != null) {
                        item.setDate_of_publish(volume.getVolumeInfo().getPublishedDate());
                    }

                    if (volume.getVolumeInfo().getImageLinks() != null && !TextUtils.isEmpty(volume.getVolumeInfo().getImageLinks().getThumbnail())) {
                        item.setPath_img(volume.getVolumeInfo().getImageLinks().getThumbnail());
                        ViewUtils.loadImage(ivItemImage, item.getPath_img());
                    }
                } else {
                    ViewUtils.toast(context, R.string.isbn_not_found, true);
                }
                DialogHelper.getInstance().dismissDialog();
            }

            @Override
            public void onFailure() {
                DialogHelper.getInstance().dismissDialog();
                ViewUtils.toast(context, R.string.isbn_not_found, true);
            }
        }, ResponseCodeHttpConstant.SUCCESS));
    }

    private final int UPLOAD_IMG_CALL = 0;
    private final int ITEM_CALL = 1;
    private final int END_CALLS = 2;
    private final int FAIL_CALLS = 3;
    private CounterMutex counterMutex;

    private void onValidate() {
        DialogHelper.getInstance().showLoading(context);

        counterMutex = new CounterMutex();
        counterMutex.getCurrentCounter().observe(((HomeActivity) context).getCurrentFragment(), count -> {
            switch (count) {
                case UPLOAD_IMG_CALL:
                    DialogHelper.getInstance().showLoading(context);
                    uploadImgRemote();
                    break;
                case ITEM_CALL:
                    if (isModifyMode) {
                        updateItemRemote();
                    } else {
                        createItemRemote();
                    }
                    break;
                case END_CALLS:
                    DialogHelper.getInstance().dismissDialog();
                    ViewUtils.toast(context, R.string.update_item_success, false);
                    dismiss();
                    break;
                case FAIL_CALLS:
                    DialogHelper.getInstance().dismissDialog();
                    break;
            }
        });

        dismiss();
    }

    private void uploadImgRemote() {
        if (imgFileToUpload == null) {
            counterMutex.incrementCounter();
            return;
        }

        MultipartBody.Part requestImg = MultipartBody.Part.createFormData(
                "img",
                imgFileToUpload.getName(),
                RequestBody.create(MediaType.parse("multipart/form-data"), imgFileToUpload));

        Call<UploadImageResponse> call = RetrofitInstance.getAPIService().insertImage(requestImg);
        call.enqueue(new CallbackEntity<>(UploadImageResponse.class, new CallbackEntity.CallbackEntityResponse<UploadImageResponse>() {
            @Override
            public void onSuccess(UploadImageResponse uploadImageResponse) {
                if (uploadImageResponse.getPath_img() != null) {
                    item.setPath_img(uploadImageResponse.getPath_img());
                    counterMutex.incrementCounter();
                }
            }

            @Override
            public void onFailure() {
                counterMutex.incrementCounter();
                ViewUtils.toast(context, R.string.upload_img_failed, true);
            }
        }, ResponseCodeHttpConstant.CREATE_SUCCESS));
    }

    private void createItemRemote() {
        HashMap<String, Object> mapItem = ItemFields.serializeForCreation(item);

        Call<ItemResponse> call = RetrofitInstance.getAPIService().createItem(mapItem);
        call.enqueue(new CallbackEntity<>(ItemResponse.class, new CallbackEntity.CallbackEntityResponse<ItemResponse>() {
            @Override
            public void onSuccess(ItemResponse itemResponse) {
                PopupCreateModifyItem.this.item = itemResponse.getEntity();
                counterMutex.incrementCounter();
            }

            @Override
            public void onFailure() {
                counterMutex.getCurrentCounter().setValue(FAIL_CALLS);
                ResponseUtils.onCallFailed(context);
            }
        }, ResponseCodeHttpConstant.CREATE_SUCCESS));
    }

    private void updateItemRemote() {
        HashMap<String, Object> mapItem = ItemFields.serializeForUpdating(item);

        Call<UpdateItemResponse> call = RetrofitInstance.getAPIService().updateItemById(item.getId(), mapItem);
        call.enqueue(new CallbackEntity<>(UpdateItemResponse.class, new CallbackEntity.CallbackEntityResponse<UpdateItemResponse>() {
            @Override
            public void onSuccess(UpdateItemResponse updateItemResponse) {
                counterMutex.incrementCounter();
            }

            @Override
            public void onFailure() {
                counterMutex.getCurrentCounter().setValue(FAIL_CALLS);
                ResponseUtils.onCallFailed(context);
            }
        }, ResponseCodeHttpConstant.SUCCESS));
    }
}












