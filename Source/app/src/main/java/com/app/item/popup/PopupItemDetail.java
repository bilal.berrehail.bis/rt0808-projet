package com.app.item.popup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;

import com.app.R;
import com.app.databinding.PopupItemDetailBinding;
import com.app.exemplary.fragment.ExemplaryItemListingFragment;
import com.app.home.HomeActivity;
import com.app.model.constant.BundleConstant;
import com.app.retrofit.model.object.Item;
import com.google.gson.Gson;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PopupItemDetail extends Dialog {
    @BindView(R.id.cl_header_card)
    ConstraintLayout clHeaderCard;
    @BindView(R.id.btn_consultation_exemplary)
    Button btnConsultationExemplarys;
    @BindView(R.id.btn_modify)
    Button btnModify;
    @BindView(R.id.btn_back)
    Button btnBack;

    private final Context context;
    private Item item;

    @SuppressLint("StaticFieldLeak")
    private static PopupItemDetail popupItemDetail;

    private PopupItemDetail(Context context, Item item) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
        try {
            this.item = (Item) item.clone();
        } catch(CloneNotSupportedException ex) {
            this.item = new Item();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);

        PopupItemDetailBinding popupItemDetailBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.popup_item_detail, null, false);
        popupItemDetailBinding.setItem(item);

        View view = popupItemDetailBinding.getRoot();

        setContentView(view);
        ButterKnife.bind(this, view);

        initView();
    }

    @Override
    public void dismiss() {
        popupItemDetail = null;
        super.dismiss();
    }

    public static PopupItemDetail getInstance(Context context, Item item) {
        if (popupItemDetail == null) {
            popupItemDetail = new PopupItemDetail(context, item);
        }
        return popupItemDetail;
    }

    private void initView() {
        btnConsultationExemplarys.setOnClickListener(v -> showExemplaryListing());
        btnModify.setOnClickListener(v -> {
            PopupCreateModifyItem popupCreateModifyItem = PopupCreateModifyItem.getInstance(context, item);
            popupCreateModifyItem.show();
            dismiss();
        });
        btnBack.setOnClickListener(v -> dismiss());
    }

    private void showExemplaryListing() {
        ExemplaryItemListingFragment exemplaryListingFragment = new ExemplaryItemListingFragment();

        Bundle bundle = new Bundle();
        bundle.putString(BundleConstant.BUNDLE_ITEM, new Gson().toJson(item));

        exemplaryListingFragment.setArguments(bundle);

        ((HomeActivity) context).pushFragment(exemplaryListingFragment);
        dismiss();
    }
}
















