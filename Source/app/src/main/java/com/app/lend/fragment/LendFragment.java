package com.app.lend.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.R;
import com.app.base.BaseFragment;
import com.app.databinding.FragmentLendBinding;
import com.app.dialog.helper.DialogHelper;
import com.app.exemplary.adapter.ExemplaryListingAdapter;
import com.app.exemplary.holder.ExemplaryListingHolder;
import com.app.model.constant.BundleConstant;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Exemplary;
import com.app.retrofit.model.object.LendInformation;
import com.app.retrofit.model.response.SubmitLendResponse;
import com.app.utils.BundleUtils;
import com.app.utils.CallbackEntity;
import com.app.utils.ViewUtils;
import com.google.gson.Gson;

import butterknife.BindView;
import retrofit2.Call;

public class LendFragment extends BaseFragment implements Toolbar.OnMenuItemClickListener {
    @BindView(R.id.ll_main_exemplary)
    LinearLayout llMainExemplary;
    @BindView(R.id.btn_select_exemplary)
    Button btnSelectExemplary;
    @BindView(R.id.iv_calendar)
    ImageView ivCalendar;
    @BindView(R.id.img_lend)
    ImageView imgLend;

    private FragmentLendBinding fragmentLendBinding;

    private Exemplary exemplary;
    private LendInformation lendInformation;

    private PopupWindow popupSelectExemplary;

    @Override
    protected int getLayout() {
        return R.layout.fragment_lend;
    }

    @Override
    protected int getTitle() {
        return R.string.page_lend;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (getArguments() != null && getArguments().containsKey(BundleConstant.BUNDLE_EXEMPLARY)) {
            exemplary = new Gson().fromJson((String) BundleUtils.getValue(getArguments(), BundleConstant.BUNDLE_EXEMPLARY), Exemplary.class);
            if (exemplary.getLendInformation() != null) {
                lendInformation = exemplary.getLendInformation();
            } else {
                lendInformation = new LendInformation();
            }
        } else {
            lendInformation = new LendInformation();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        homeActivity.inflateToolbarMenu(R.menu.menu_lend, this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentLendBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_lend, container, false);
        View root = fragmentLendBinding.getRoot();
        fragmentLendBinding.setExemplary(exemplary);
        fragmentLendBinding.setLendInfo(lendInformation);

        return root;
    }

    @Override
    protected void onViewCompleteLoaded() {
        super.onViewCompleteLoaded();
        initExemplaryAdapter();
    }

    @Override
    protected void afterActivityCreated() {
        initView();
    }

    private void initView() {
        btnSelectExemplary.setOnClickListener(v -> {
            if (popupSelectExemplary != null) {
                popupSelectExemplary.showAsDropDown(llMainExemplary);
                ViewUtils.hideKeyboard(homeActivity);
            }
        });

        ivCalendar.setOnClickListener(v -> ViewUtils.showDateTimePicker(homeActivity, calendar -> lendInformation.setDate_limit(calendar.getTime().getTime())));

        Animation shake = AnimationUtils.loadAnimation(homeActivity, R.anim.shake);
        imgLend.setOnClickListener(v -> imgLend.startAnimation(shake));
    }

    private void initExemplaryAdapter() {
        ExemplaryListingAdapter exemplaryListingAdapter = new ExemplaryListingAdapter(homeActivity, null, false, false, new ExemplaryListingHolder.ExemplaryListingListener() {
            @Override
            public void onClick(Exemplary exemplary) {
                if (exemplary.getLendInformation() != null) {
                    ViewUtils.toast(homeActivity, R.string.exemplary_already_lent, true);
                    return;
                }
                LendFragment.this.exemplary = exemplary;
                LendFragment.this.fragmentLendBinding.setExemplary(exemplary);
                if (popupSelectExemplary != null) {
                    popupSelectExemplary.dismiss();
                }
            }

            @Override
            public void onLend(Exemplary exemplary) {

            }

            @Override
            public void onMoveRequest(Exemplary exemplary) {
            }
        });

        popupSelectExemplary = ViewUtils.createPopupMenu(homeActivity, llMainExemplary, exemplaryListingAdapter, new LinearLayoutManager(homeActivity));
    }

    private void onValidate() {
        if (exemplary == null) {
            ViewUtils.toast(homeActivity, R.string.select_exemplary, true);
            return;
        }
        if (lendInformation.getDate_limit() == null) {
            ViewUtils.toast(homeActivity, R.string.select_date, true);
            return;
        }
        if (lendInformation.getReceiver_name() == null) {
            ViewUtils.toast(homeActivity, R.string.select_receiver_name, true);
            return;
        }

        DialogHelper.getInstance().showLoading(homeActivity);

        Call<SubmitLendResponse> call = RetrofitInstance.getAPIService().submitExemplaryLend(exemplary.getId(), lendInformation.getReceiver_name(), lendInformation.getDate_limit(), lendInformation.getNote());
        call.enqueue(new CallbackEntity<>(SubmitLendResponse.class, new CallbackEntity.CallbackEntityResponse<SubmitLendResponse>() {
            @Override
            public void onSuccess(SubmitLendResponse submitLendResponse) {
                DialogHelper.getInstance().dismissDialog();
                if (submitLendResponse != null) {
                    if (submitLendResponse.isSuccessful()) {
                        if (submitLendResponse.getEntity() != null) {
                            exemplary = submitLendResponse.getEntity();
                            fragmentLendBinding.setExemplary(exemplary);
                            ViewUtils.toast(homeActivity, R.string.lend_successful, false);
                            homeActivity.popBackStack();
                            return;
                        }
                    }
                }
                ViewUtils.toast(homeActivity, R.string.lend_failed, true);
            }

            @Override
            public void onFailure() {
                DialogHelper.getInstance().dismissDialog();
                ViewUtils.toast(homeActivity, R.string.lend_failed, true);
            }
        }, ResponseCodeHttpConstant.SUCCESS, ResponseCodeHttpConstant.BAD_REQUEST, ResponseCodeHttpConstant.CONFLIT));
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.action_validate) {
            onValidate();
            return true;
        }
        return false;
    }
}
