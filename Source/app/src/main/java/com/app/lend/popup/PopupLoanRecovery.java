package com.app.lend.popup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.R;
import com.app.databinding.PopupRecoveryLendBinding;
import com.app.dialog.helper.DialogHelper;
import com.app.location.adapter.LocationListingAdapter;
import com.app.location.holder.LocationListingHolder;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Exemplary;
import com.app.retrofit.model.object.Location;
import com.app.retrofit.model.response.RecoveryLendResponse;
import com.app.utils.CallbackEntity;
import com.app.utils.ViewUtils;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class PopupLoanRecovery extends Dialog {
    @BindView(R.id.ll_item_infos)
    LinearLayout llItemInfos;
    @BindView(R.id.iv_edit_location_arrival)
    ImageView ivEditLocationArrival;
    @BindView(R.id.btn_recovery)
    Button btnRecovery;
    @BindView(R.id.btn_cancel)
    Button btnCancel;

    private final Context context;
    private final Exemplary exemplary;

    private PopupWindow popupSelectLocation;

    @SuppressLint("StaticFieldLeak")
    private static PopupLoanRecovery popupLoanRecovery;

    private PopupLoanRecovery(Context context, Exemplary exemplary) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
        this.exemplary = exemplary;
        if (exemplary.getLendInformation() == null) {
            ViewUtils.toast(context, R.string.exemplary_not_lent, true);
            dismiss();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);

        PopupRecoveryLendBinding popupRecoveryLendBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.popup_recovery_lend, null, false);
        popupRecoveryLendBinding.setExemplary(exemplary);

        View view = popupRecoveryLendBinding.getRoot();

        setContentView(view);
        ButterKnife.bind(this, view);

        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                initLocationAdapter();
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
        initView();
    }

    @Override
    public void dismiss() {
        popupLoanRecovery = null;
        super.dismiss();
    }

    public static PopupLoanRecovery getInstance(Context context, Exemplary exemplary) {
        if (popupLoanRecovery == null) {
            popupLoanRecovery = new PopupLoanRecovery(context, exemplary);
        }
        return popupLoanRecovery;
    }

    private void initLocationAdapter() {
        LocationListingAdapter locationListingAdapter = new LocationListingAdapter(context, null, false, false, false, new LocationListingHolder.LocationListingListener() {
            @Override
            public void onClick(Location location) {
                if (popupSelectLocation != null) {
                    exemplary.setLocation(location);
                    popupSelectLocation.dismiss();
                }
            }

            @Override
            public void onDelete(Location location) {
            }

            @Override
            public void onConsultationExemplarys(Location location) {
            }
        });
        popupSelectLocation = ViewUtils.createPopupMenu(context, llItemInfos, locationListingAdapter, new LinearLayoutManager(context));
    }

    private void initView() {
        ivEditLocationArrival.setOnClickListener(v -> {
            if (popupSelectLocation != null) {
                popupSelectLocation.showAsDropDown(llItemInfos);
                ViewUtils.hideKeyboard(getWindow());
            }
        });

        btnRecovery.setOnClickListener(v -> {
            if (exemplary.getLocation() == null) {
                ViewUtils.toast(context, R.string.select_location, true);
                return;
            }
            DialogHelper.getInstance().showLoading(context);

            Call<RecoveryLendResponse> call = RetrofitInstance.getAPIService().recoveryExemplaryLend(exemplary.getId(), exemplary.getLocation().getId());
            call.enqueue(new CallbackEntity<>(RecoveryLendResponse.class, new CallbackEntity.CallbackEntityResponse<RecoveryLendResponse>() {
                @Override
                public void onSuccess(RecoveryLendResponse recoveryLendResponse) {
                    DialogHelper.getInstance().dismissDialog();
                    if (recoveryLendResponse != null) {
                        if (recoveryLendResponse.isSuccessful()) {
                            exemplary.setLendInformation(null);
                            ViewUtils.toast(context, R.string.recovery_successful, false);
                            dismiss();
                            return;
                        }
                    }
                    ViewUtils.toast(context, R.string.recovery_failed, true);
                }

                @Override
                public void onFailure() {
                    DialogHelper.getInstance().dismissDialog();
                    ViewUtils.toast(context, R.string.recovery_failed, true);
                }
            }, ResponseCodeHttpConstant.SUCCESS, ResponseCodeHttpConstant.CONFLIT));
        });

        btnCancel.setOnClickListener(v -> dismiss());
    }
}
