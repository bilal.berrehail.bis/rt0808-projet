package com.app.library.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.R;
import com.app.databinding.ObjectLibraryListingBinding;
import com.app.library.holder.LibraryListingHolder;
import com.app.retrofit.model.object.Library;
import com.app.retrofit.model.response.ListLibraryResponse;
import com.app.retrofit.utils.EntityUtils;
import com.app.view.PaginationAdapter;

import io.reactivex.Observable;

import static com.app.retrofit.utils.EntityUtils.LIMIT_GET_LIBRARIES_PER_PAGE;

public class LibraryListingAdapter extends PaginationAdapter<ListLibraryResponse, LibraryListingHolder> {
    private final LibraryListingHolder.LibraryListingListener libraryListingListener;

    public LibraryListingAdapter(Context context, SwipeRefreshLayout swipeRefreshLayout, LibraryListingHolder.LibraryListingListener libraryListingListener) {
        super(context, swipeRefreshLayout);
        this.libraryListingListener = libraryListingListener;

        start(this);
    }

    @NonNull
    @Override
    public LibraryListingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ObjectLibraryListingBinding objectLibraryListingBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.object_library_listing, parent, false);
        return new LibraryListingHolder(objectLibraryListingBinding, libraryListingListener);
    }

    @Override
    public void onBindViewHolder(@NonNull LibraryListingHolder holder, int position) {
        holder.bind((Library) entities.get(position));
    }

    @Override
    protected Observable<ListLibraryResponse> getObservable() {
        return EntityUtils.getLibrariesRx(currentPage, LIMIT_GET_LIBRARIES_PER_PAGE, keyword);
    }
}
