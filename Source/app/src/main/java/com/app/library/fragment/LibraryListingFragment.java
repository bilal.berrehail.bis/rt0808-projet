package com.app.library.fragment;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.R;
import com.app.base.BaseFragment;
import com.app.home.HomeFragment;
import com.app.library.adapter.LibraryListingAdapter;
import com.app.library.holder.LibraryListingHolder;
import com.app.library.popup.PopupCreateModifyLibrary;
import com.app.library.popup.PopupJoinLibrary;
import com.app.model.UserInstance;
import com.app.model.constant.BundleConstant;
import com.app.model.constant.EnumRole;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.model.decoration.MarginItemDecoration;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Library;
import com.app.retrofit.model.response.LoginLibraryResponse;
import com.app.retrofit.utils.ResponseUtils;
import com.app.utils.BundleUtils;
import com.app.utils.CallbackEntity;
import com.app.utils.ViewUtils;

import butterknife.BindView;
import butterknife.OnTextChanged;
import retrofit2.Call;

public class LibraryListingFragment extends BaseFragment implements Toolbar.OnMenuItemClickListener, LibraryListingHolder.LibraryListingListener {
    @BindView(R.id.et_search_bar)
    EditText etSearchBar;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.rv_libraries)
    RecyclerView rvLibraries;

    private LibraryListingAdapter libraryListingAdapter;

    @Override
    protected int getLayout() {
        return R.layout.fragment_library_listing;
    }

    @Override
    protected int getTitle() {
        return R.string.consultation_library;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        rvLibraries.setAdapter(null);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            etSearchBar.setText("");
        }
    }

    @Override
    public void remoteCall(Bundle bundle) {
        if (BundleUtils.getValue(bundle, BundleConstant.BUNDLE_UPDATE) != null) {
            etSearchBar.setText("");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        homeActivity.inflateToolbarMenu(R.menu.menu_library_listing, this);
    }

    @Override
    protected void afterActivityCreated() {
        libraryListingAdapter = new LibraryListingAdapter(homeActivity, swipeRefresh, this);
        rvLibraries.setLayoutManager(new LinearLayoutManager(homeActivity));
        rvLibraries.addItemDecoration(new MarginItemDecoration(8));
        rvLibraries.setAdapter(libraryListingAdapter);
    }

    @OnTextChanged(R.id.et_search_bar)
    public void searchBarTextChanged() {
        libraryListingAdapter.refreshKeyword(etSearchBar.getText().toString());
    }

    @Override
    public void onConnect(Library library) {
        Call<LoginLibraryResponse> call = RetrofitInstance.getAPIService().loginLibrary(library.getId());
        call.enqueue(new CallbackEntity<>(LoginLibraryResponse.class, new CallbackEntity.CallbackEntityResponse<LoginLibraryResponse>() {
            @Override
            public void onSuccess(LoginLibraryResponse loginLibraryResponse) {
                if (loginLibraryResponse.getCode_response().equals(ResponseCodeHttpConstant.UNAUTHORIZED)) {
                    ViewUtils.toast(homeActivity, R.string.login_library_failed, true);
                } else if (loginLibraryResponse.getCode_response().equals(ResponseCodeHttpConstant.SUCCESS)) {
                    UserInstance.getUser().setLibrary(library);
                    UserInstance.getUser().setRole(EnumRole.getEnumRoleFromRole(loginLibraryResponse.getRole()));
                    UserInstance.getUser().setToken(loginLibraryResponse.getNew_token());

                    RetrofitInstance.loadBuilderRetrofit();

                    homeActivity.popBackStack();
                    HomeFragment homeFragment = new HomeFragment();
                    homeActivity.pushFragment(homeFragment);
                }
            }

            @Override
            public void onFailure() {
                ResponseUtils.onCallFailed(homeActivity);
            }
        }, ResponseCodeHttpConstant.SUCCESS, ResponseCodeHttpConstant.UNAUTHORIZED));
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                PopupCreateModifyLibrary popupCreateModifyLibrary = PopupCreateModifyLibrary.getInstance(homeActivity);
                popupCreateModifyLibrary.show();
                return true;
            case R.id.action_join:
                PopupJoinLibrary popupJoinLibrary = PopupJoinLibrary.getInstance(homeActivity);
                popupJoinLibrary.show();
                return true;
            case R.id.action_signout:
                homeActivity.backToLogin();
                break;
        }
        return false;
    }
}
