package com.app.library.holder;

import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.R;
import com.app.databinding.ObjectLibraryListingBinding;
import com.app.retrofit.model.object.Library;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LibraryListingHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_library_connect)
    ImageView ivLibraryConnect;

    private final LibraryListingListener LibraryListingListener;
    private final ObjectLibraryListingBinding objectLibraryListingBinding;

    public LibraryListingHolder(ObjectLibraryListingBinding objectLibraryListingBinding, LibraryListingListener LibraryListingListener) {
        super(objectLibraryListingBinding.getRoot());
        ButterKnife.bind(this, itemView);

        this.objectLibraryListingBinding = objectLibraryListingBinding;
        this.LibraryListingListener = LibraryListingListener;
    }

    public void bind(Library library) {
        objectLibraryListingBinding.setLibrary(library);
        ivLibraryConnect.setOnClickListener(v -> LibraryListingListener.onConnect(library));
    }

    public interface LibraryListingListener {
        void onConnect(Library library);
    }
}
