package com.app.library.popup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;

import com.app.R;
import com.app.databinding.PopupCreateModifyLibraryBinding;
import com.app.dialog.helper.DialogHelper;
import com.app.home.HomeActivity;
import com.app.model.constant.BundleConstant;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.model.fields.LibraryFields;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Library;
import com.app.retrofit.model.response.LibraryResponse;
import com.app.retrofit.model.response.UpdateLibraryResponse;
import com.app.retrofit.model.response.UploadImageResponse;
import com.app.retrofit.utils.ResponseUtils;
import com.app.utils.CallbackEntity;
import com.app.utils.CounterMutex;
import com.app.utils.StorageUtils;
import com.app.utils.ViewUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class PopupCreateModifyLibrary extends Dialog {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_library_image)
    ImageView ivLibraryImage;
    @BindView(R.id.iv_edit_img)
    ImageView ivEditImg;
    @BindView(R.id.iv_copy)
    ImageView ivCopy;
    @BindView(R.id.et_library_token)
    EditText etLibraryToken;
    @BindView(R.id.btn_create_modify)
    Button btnCreateModify;
    @BindView(R.id.btn_cancel)
    Button btnCancel;

    private final Context context;
    private Library library;

    private boolean isModifyMode;

    private PopupCreateModifyLibraryBinding popupCreateModifyLibraryBinding;

    private File imgFileToUpload;

    @SuppressLint("StaticFieldLeak")
    private static PopupCreateModifyLibrary popupCreateModifyLibrary;

    private PopupCreateModifyLibrary(Context context) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
        this.library = new Library();

        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);

        popupCreateModifyLibraryBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.popup_create_modify_library, null, false);

        popupCreateModifyLibraryBinding.setLibrary(library);

        View view = popupCreateModifyLibraryBinding.getRoot();
        ButterKnife.bind(this, view);

        setContentView(view);
        initView();
    }

    private PopupCreateModifyLibrary(Context context, Library library) {
        this(context);
        setModifyMode(library);
    }

    private void setModifyMode(Library library) {
        this.library = library;
        popupCreateModifyLibraryBinding.setLibrary(library);

        isModifyMode = true;

        tvTitle.setText(R.string.modify_library);
        btnCreateModify.setText(R.string.modify);
    }

    public static PopupCreateModifyLibrary getInstance(Context context) {
        if (popupCreateModifyLibrary == null) {
            popupCreateModifyLibrary = new PopupCreateModifyLibrary(context);
        }
        return popupCreateModifyLibrary;
    }

    public static PopupCreateModifyLibrary getInstance(Context context, Library library) {
        if (popupCreateModifyLibrary == null) {
            popupCreateModifyLibrary = new PopupCreateModifyLibrary(context, library);
        }
        return popupCreateModifyLibrary;
    }

    @Override
    public void dismiss() {
        Bundle bundleUpdate = new Bundle();
        bundleUpdate.putBoolean(BundleConstant.BUNDLE_UPDATE, true);
        ((HomeActivity) context).remoteCallCurrentFragment(bundleUpdate);

        popupCreateModifyLibrary = null;
        super.dismiss();
    }

    private void initView() {
        ivCopy.setOnClickListener(v -> {
            ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            if (clipboardManager != null) {
                clipboardManager.setPrimaryClip(ClipData.newPlainText("token", etLibraryToken.getText()));
                ViewUtils.toast(context, R.string.copy_clipboard, false);
            }
        });

        ivEditImg.setOnClickListener(v -> {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            ((HomeActivity) context).remoteCallActivityResult(gallery, 100, intent -> {

                if (intent.getData() != null && intent.getData().getPath() != null) {
                    imgFileToUpload = StorageUtils.moveFileToLocalDirectory(context, intent.getData());

                    if (imgFileToUpload == null) {
                        ViewUtils.toast(context, R.string.error_load_image, true);
                        return;
                    }

                    ViewUtils.loadImage(ivLibraryImage, intent.getData());
                }

            });
        });

        btnCancel.setOnClickListener(v -> dismiss());

        btnCreateModify.setOnClickListener(v -> onValidate());
    }

    private final int UPLOAD_IMG_CALL = 0;
    private final int LIBRARY_CALL = 1;
    private final int END_CALLS = 2;
    private final int FAIL_CALLS = 3;
    private CounterMutex counterMutex;

    private void onValidate() {
        DialogHelper.getInstance().showLoading(context);

        counterMutex = new CounterMutex();
        counterMutex.getCurrentCounter().observe(((HomeActivity) context).getCurrentFragment(), count -> {
            switch (count) {
                case UPLOAD_IMG_CALL:
                    DialogHelper.getInstance().showLoading(context);
                    uploadImgRemote();
                    break;
                case LIBRARY_CALL:
                    if (isModifyMode) {
                        updateLibraryRemote();
                    } else {
                        createLibrayRemote();
                    }
                    break;
                case END_CALLS:
                    DialogHelper.getInstance().dismissDialog();
                    if (isModifyMode) {
                        ViewUtils.toast(context, R.string.update_library_success, false);
                    } else {
                        ViewUtils.toast(context, R.string.create_library, false);
                    }
                    dismiss();
                    break;
                case FAIL_CALLS:
                    DialogHelper.getInstance().dismissDialog();
                    break;
            }
        });

        dismiss();
    }

    private void uploadImgRemote() {
        if (imgFileToUpload == null) {
            counterMutex.incrementCounter();
            return;
        }

        MultipartBody.Part requestImg = MultipartBody.Part.createFormData(
                "img",
                imgFileToUpload.getName(),
                RequestBody.create(MediaType.parse("multipart/form-data"), imgFileToUpload));

        Call<UploadImageResponse> call = RetrofitInstance.getAPIService().insertImage(requestImg);
        call.enqueue(new CallbackEntity<>(UploadImageResponse.class, new CallbackEntity.CallbackEntityResponse<UploadImageResponse>() {
            @Override
            public void onSuccess(UploadImageResponse uploadImageResponse) {
                if (uploadImageResponse.getPath_img() != null) {
                    library.setPath_img(uploadImageResponse.getPath_img());
                    counterMutex.incrementCounter();
                }
            }

            @Override
            public void onFailure() {
                counterMutex.incrementCounter();
                ViewUtils.toast(context, R.string.upload_img_failed, true);
            }
        }, ResponseCodeHttpConstant.CREATE_SUCCESS));
    }

    private void createLibrayRemote() {
        HashMap<String, Object> mapLibrary = LibraryFields.serializeForCreation(library);

        Call<LibraryResponse> call = RetrofitInstance.getAPIService().createLibrary(mapLibrary);
        call.enqueue(new CallbackEntity<>(LibraryResponse.class, new CallbackEntity.CallbackEntityResponse<LibraryResponse>() {
            @Override
            public void onSuccess(LibraryResponse libraryResponse) {
                if (libraryResponse.getCode_response().equals(ResponseCodeHttpConstant.UNAUTHORIZED)) {
                    ViewUtils.toast(context, R.string.unauthorized, true);
                    counterMutex.getCurrentCounter().setValue(FAIL_CALLS);
                } else if (libraryResponse.getCode_response().equals(ResponseCodeHttpConstant.SUCCESS)) {
                    PopupCreateModifyLibrary.this.library = libraryResponse.getEntity();
                    counterMutex.incrementCounter();
                }
            }

            @Override
            public void onFailure() {
                counterMutex.getCurrentCounter().setValue(FAIL_CALLS);
                ResponseUtils.onCallFailed(context);
            }
        }, ResponseCodeHttpConstant.SUCCESS, ResponseCodeHttpConstant.UNAUTHORIZED));
    }

    private void updateLibraryRemote() {
        HashMap<String, Object> mapItem = LibraryFields.serializeForUpdating(library);

        Call<UpdateLibraryResponse> call = RetrofitInstance.getAPIService().updateLibraryById(mapItem);
        call.enqueue(new CallbackEntity<>(UpdateLibraryResponse.class, new CallbackEntity.CallbackEntityResponse<UpdateLibraryResponse>() {
            @Override
            public void onSuccess(UpdateLibraryResponse updateLibraryResponse) {
                counterMutex.incrementCounter();
            }

            @Override
            public void onFailure() {
                counterMutex.getCurrentCounter().setValue(END_CALLS);
                ResponseUtils.onCallFailed(context);
            }
        }, ResponseCodeHttpConstant.SUCCESS));
    }
}
