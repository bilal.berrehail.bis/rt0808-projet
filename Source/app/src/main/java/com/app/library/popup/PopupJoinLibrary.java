package com.app.library.popup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.R;
import com.app.home.HomeActivity;
import com.app.model.constant.BundleConstant;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.response.JoinLibraryResponse;
import com.app.retrofit.utils.ResponseUtils;
import com.app.utils.CallbackEntity;
import com.app.utils.ViewUtils;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class PopupJoinLibrary extends Dialog {
    @BindView(R.id.et_token)
    EditText etToken;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_join)
    Button btnJoin;

    private final Context context;

    @SuppressLint("StaticFieldLeak")
    private static PopupJoinLibrary popupJoinLibrary;

    private PopupJoinLibrary(Context context) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);

        View view = LayoutInflater.from(context).inflate(R.layout.popup_join_library, null);

        ButterKnife.bind(this, view);

        setContentView(view);
        initView();
    }

    public static PopupJoinLibrary getInstance(Context context) {
        if (popupJoinLibrary == null) {
            popupJoinLibrary = new PopupJoinLibrary(context);
        }
        return popupJoinLibrary;
    }

    @Override
    public void dismiss() {
        Bundle bundleUpdate = new Bundle();
        bundleUpdate.putBoolean(BundleConstant.BUNDLE_UPDATE, true);
        ((HomeActivity) context).remoteCallCurrentFragment(bundleUpdate);

        popupJoinLibrary = null;
        super.dismiss();
    }

    private void initView() {
        btnCancel.setOnClickListener(v -> dismiss());

        btnJoin.setOnClickListener(v -> onJoin());
    }

    private void onJoin() {
        Call<JoinLibraryResponse> call = RetrofitInstance.getAPIService().joinLibrary(etToken.toString());
        call.enqueue(new CallbackEntity<>(JoinLibraryResponse.class, new CallbackEntity.CallbackEntityResponse<JoinLibraryResponse>() {
            @Override
            public void onSuccess(JoinLibraryResponse joinLibraryResponse) {
                if (joinLibraryResponse.getCode_response().equals(ResponseCodeHttpConstant.SUCCESS)) {
                    ViewUtils.toast(context, R.string.join_library_success, false);
                    dismiss();
                } else if (joinLibraryResponse.getCode_response().equals(ResponseCodeHttpConstant.FORBIDDEN)) {
                    ViewUtils.toast(context, R.string.token_incorrect, true);
                } else if (joinLibraryResponse.getCode_response().equals(ResponseCodeHttpConstant.UNAUTHORIZED)) {
                    ViewUtils.toast(context, R.string.unauthorized, true);
                }
            }

            @Override
            public void onFailure() {
                ResponseUtils.onCallFailed(context);
            }
        }, ResponseCodeHttpConstant.SUCCESS, ResponseCodeHttpConstant.FORBIDDEN, ResponseCodeHttpConstant.UNAUTHORIZED));
    }
}
