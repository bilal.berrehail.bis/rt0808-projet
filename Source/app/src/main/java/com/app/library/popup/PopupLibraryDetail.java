package com.app.library.popup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.databinding.DataBindingUtil;

import com.app.R;
import com.app.databinding.PopupLibraryDetailBinding;
import com.app.model.UserInstance;
import com.app.retrofit.model.object.Library;
import com.app.worker.popup.PopupWorkerListing;
import com.app.utils.ViewUtils;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PopupLibraryDetail extends Dialog {
    @BindView(R.id.iv_user_listing)
    ImageView ivUserListing;
    @BindView(R.id.btn_back)
    Button btnBack;
    @BindView(R.id.btn_modify)
    Button btnModify;

    private final Context context;
    private final Library library;

    @SuppressLint("StaticFieldLeak")
    private static PopupLibraryDetail popupLibraryDetail;

    private PopupLibraryDetail(Context context, Library library) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
        this.library = library;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);

        PopupLibraryDetailBinding popupLibraryDetailBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.popup_library_detail, null, false);

        popupLibraryDetailBinding.setLibrary(library);

        View view = popupLibraryDetailBinding.getRoot();
        ButterKnife.bind(this, view);

        setContentView(view);
        initView();
    }

    public static PopupLibraryDetail getInstance(Context context, Library library) {
        if (popupLibraryDetail == null) {
            popupLibraryDetail = new PopupLibraryDetail(context, library);
        }
        return popupLibraryDetail;
    }

    @Override
    public void dismiss() {
        popupLibraryDetail = null;
        super.dismiss();
    }

    private void initView() {
        ivUserListing.setOnClickListener(v -> {
            PopupWorkerListing popupWorkerListing = PopupWorkerListing.getInstance(context);
            popupWorkerListing.show();
        });

        btnBack.setOnClickListener(v -> dismiss());

        btnModify.setOnClickListener(v -> {
            if (UserInstance.getUser().getRole().isModifyLibrary()) {
                PopupCreateModifyLibrary popupCreateModifyLibrary = PopupCreateModifyLibrary.getInstance(context, library);
                popupCreateModifyLibrary.show();
                dismiss();
            } else {
                ViewUtils.toast(context, R.string.unauthorized, true);
            }
        });
    }
}
