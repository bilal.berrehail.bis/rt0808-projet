package com.app.location.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.R;
import com.app.databinding.ObjectLocationListingBinding;
import com.app.location.holder.LocationListingHolder;
import com.app.retrofit.model.object.Location;
import com.app.retrofit.model.response.ListLocationResponse;
import com.app.retrofit.utils.EntityUtils;
import com.app.view.PaginationAdapter;

import io.reactivex.Observable;

public class LocationListingAdapter extends PaginationAdapter<ListLocationResponse, LocationListingHolder> {
    private final LocationListingHolder.LocationListingListener locationListingListener;
    private final boolean deleteMode;
    private final boolean countMode;
    private final boolean openStockMode;

    public LocationListingAdapter(Context context, SwipeRefreshLayout swipeRefreshLayout, boolean deleteMode, boolean countMode, boolean openStockMode, LocationListingHolder.LocationListingListener locationListingListener) {
        super(context, swipeRefreshLayout);
        this.locationListingListener = locationListingListener;
        this.deleteMode = deleteMode;
        this.countMode = countMode;
        this.openStockMode = openStockMode;

        start(this);
    }

    @NonNull
    @Override
    public LocationListingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ObjectLocationListingBinding objectLocationListingBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.object_location_listing, parent, false);
        return new LocationListingHolder(objectLocationListingBinding, deleteMode, countMode, openStockMode, locationListingListener);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationListingHolder holder, int position) {
        holder.bind((Location) entities.get(position));
    }

    @Override
    protected Observable<ListLocationResponse> getObservable() {
        return EntityUtils.getLocationsRx(currentPage, EntityUtils.LIMIT_GET_LOCATIONS_PER_PAGE, keyword);
    }
}
