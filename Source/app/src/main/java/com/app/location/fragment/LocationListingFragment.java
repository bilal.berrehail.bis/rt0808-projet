package com.app.location.fragment;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.R;
import com.app.base.BaseFragment;
import com.app.dialog.PopupConfirmCancel;
import com.app.dialog.helper.DialogHelper;
import com.app.exemplary.fragment.ExemplaryLocationListingFragment;
import com.app.location.adapter.LocationListingAdapter;
import com.app.location.holder.LocationListingHolder;
import com.app.location.popup.PopupCreateLocation;
import com.app.model.constant.BundleConstant;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.model.decoration.MarginItemDecoration;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Location;
import com.app.retrofit.model.response.DeleteLocationResponse;
import com.app.retrofit.utils.ResponseUtils;
import com.app.utils.BundleUtils;
import com.app.utils.CallbackEntity;
import com.app.utils.ViewUtils;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.OnTextChanged;
import retrofit2.Call;

public class LocationListingFragment extends BaseFragment implements Toolbar.OnMenuItemClickListener, LocationListingHolder.LocationListingListener {
    @BindView(R.id.et_search_bar)
    EditText etSearchBar;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.rv_locations)
    RecyclerView rvLocations;

    private LocationListingAdapter locationListingAdapter;

    @Override
    protected int getLayout() {
        return R.layout.fragment_location_listing;
    }

    @Override
    protected int getTitle() {
        return R.string.consultation_locations;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        rvLocations.setAdapter(null);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        etSearchBar.setText("");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        homeActivity.inflateToolbarMenu(R.menu.menu_location_listing, this);
    }

    @Override
    protected void afterActivityCreated() {
        initLocationAdapter();
    }

    private void initLocationAdapter() {
        if (locationListingAdapter == null) {
            locationListingAdapter = new LocationListingAdapter(homeActivity, swipeRefresh, true, true, true, this);
            rvLocations.setLayoutManager(new LinearLayoutManager(homeActivity));
            rvLocations.addItemDecoration(new MarginItemDecoration(8));
            rvLocations.setAdapter(locationListingAdapter);
        }
    }

    @OnTextChanged(R.id.et_search_bar)
    public void searchBarTextChanged() {
        locationListingAdapter.refreshKeyword(etSearchBar.getText().toString());
    }

    @Override
    public void onClick(Location location) {

    }

    @Override
    public void onDelete(Location location) {
        PopupConfirmCancel popupConfirmCancel = PopupConfirmCancel.getInstance(homeActivity, R.string.confirm_delete_location, () -> {
            DialogHelper.getInstance().showLoading(homeActivity);

            Call<DeleteLocationResponse> call = RetrofitInstance.getAPIService().deleteLocationById(location.getId());
            call.enqueue(new CallbackEntity<>(DeleteLocationResponse.class, new CallbackEntity.CallbackEntityResponse<DeleteLocationResponse>() {
                @Override
                public void onSuccess(DeleteLocationResponse deleteLocationResponse) {
                    DialogHelper.getInstance().dismissDialog();
                    if (deleteLocationResponse.isSuccessful()) {
                        if (locationListingAdapter.remove(location)) {
                            ViewUtils.toast(homeActivity, R.string.location_deleted, false);
                        } else {
                            ResponseUtils.onCallFailed(homeActivity);
                        }
                    } else {
                        if (deleteLocationResponse.getTotal_exemplary_related() != null) {
                            ViewUtils.toast(homeActivity, getString(R.string.error_deleting_location, deleteLocationResponse.getTotal_exemplary_related()), true);
                        }
                    }
                }

                @Override
                public void onFailure() {
                    DialogHelper.getInstance().dismissDialog();
                    ResponseUtils.onCallFailed(homeActivity);
                }
            }, ResponseCodeHttpConstant.SUCCESS, ResponseCodeHttpConstant.METHOD_NOT_ALLOWED));
        });
        popupConfirmCancel.show();
    }

    @Override
    public void onConsultationExemplarys(Location location) {
        ExemplaryLocationListingFragment stockListingFragment = new ExemplaryLocationListingFragment();

        Bundle bundle = new Bundle();
        bundle.putString(BundleConstant.BUNDLE_LOCATION, new Gson().toJson(location));
        stockListingFragment.setArguments(bundle);

        homeActivity.pushFragment(stockListingFragment);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            PopupCreateLocation popupCreateLocation = PopupCreateLocation.getInstance(homeActivity, location -> etSearchBar.setText(""));
            popupCreateLocation.show();
            return true;
        }
        return false;
    }
}
