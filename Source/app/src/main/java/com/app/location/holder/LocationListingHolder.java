package com.app.location.holder;

import android.widget.Button;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.R;
import com.app.databinding.ObjectLocationListingBinding;
import com.app.retrofit.model.object.Location;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationListingHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_location_delete)
    ImageView ivLocationDelete;
    @BindView(R.id.btn_consultation_exemplary)
    Button btnConsultationExemplarys;

    private final ObjectLocationListingBinding objectLocationListingBinding;
    private final LocationListingListener locationListingListener;

    public LocationListingHolder(ObjectLocationListingBinding objectLocationListingBinding, boolean deleteMode, boolean countMode, boolean openStockMode, LocationListingListener locationListingListener) {
        super(objectLocationListingBinding.getRoot());
        this.objectLocationListingBinding = objectLocationListingBinding;
        this.locationListingListener = locationListingListener;
        objectLocationListingBinding.setDeleteMode(deleteMode);
        objectLocationListingBinding.setCountMode(countMode);
        objectLocationListingBinding.setOpenStockMode(openStockMode);

        ButterKnife.bind(this, itemView);
    }

    public void bind(Location location) {
        objectLocationListingBinding.setLocation(location);

        itemView.setOnClickListener(v -> locationListingListener.onClick(location));
        ivLocationDelete.setOnClickListener(v -> locationListingListener.onDelete(location));
        btnConsultationExemplarys.setOnClickListener(v -> locationListingListener.onConsultationExemplarys(location));
    }

    public interface LocationListingListener {
        void onClick(Location location);

        void onDelete(Location location);

        void onConsultationExemplarys(Location location);
    }
}
