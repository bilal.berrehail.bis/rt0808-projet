package com.app.location.popup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.R;
import com.app.dialog.helper.DialogHelper;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.model.fields.LocationFields;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Location;
import com.app.retrofit.model.response.LocationResponse;
import com.app.utils.CallbackEntity;
import com.app.utils.ViewUtils;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class PopupCreateLocation extends Dialog {
    @BindView(R.id.et_location_name)
    EditText etLocationName;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_create)
    Button btnCreate;

    private final Context context;

    private final CreateLocationListener createLocationListener;

    @SuppressLint("StaticFieldLeak")
    private static PopupCreateLocation popupCreateLocation;

    private PopupCreateLocation(Context context, CreateLocationListener createLocationListener) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
        this.createLocationListener = createLocationListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_create_location, null);

        ButterKnife.bind(this, view);

        setContentView(view);
        initView();
    }

    @Override
    public void dismiss() {
        popupCreateLocation = null;
        super.dismiss();
    }

    private void initView() {
        btnCancel.setOnClickListener(v -> dismiss());
        btnCreate.setOnClickListener(v -> {
            Location newLocation = new Location();
            newLocation.setLabel(etLocationName.getText().toString());

            DialogHelper.getInstance().showLoading(context);

            HashMap<String, Object> mapLocation = LocationFields.serializeForCreation(newLocation);

            Call<LocationResponse> call = RetrofitInstance.getAPIService().createLocation(mapLocation);
            call.enqueue(new CallbackEntity<>(LocationResponse.class, new CallbackEntity.CallbackEntityResponse<LocationResponse>() {
                @Override
                public void onSuccess(LocationResponse locationResponse) {
                    DialogHelper.getInstance().dismissDialog();

                    Location location = locationResponse.getEntity();
                    newLocation.setId(location.getId());

                    ViewUtils.toast(context, R.string.location_created, false);
                    createLocationListener.onSuccess(location);
                    dismiss();
                }

                @Override
                public void onFailure() {
                    DialogHelper.getInstance().dismissDialog();
                    ViewUtils.toast(context, R.string.error_create_location, true);
                    dismiss();
                }
            }, ResponseCodeHttpConstant.CREATE_SUCCESS));
        });
    }

    public static PopupCreateLocation getInstance(Context context, CreateLocationListener createLocationListener) {
        if (popupCreateLocation == null) {
            popupCreateLocation = new PopupCreateLocation(context, createLocationListener);
        }
        return popupCreateLocation;
    }

    public interface CreateLocationListener {
        void onSuccess(Location location);
    }
}
