package com.app.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.R;
import com.app.dialog.helper.DialogHelper;
import com.app.home.HomeActivity;
import com.app.model.UserInstance;
import com.app.model.constant.EnumRole;
import com.app.model.constant.IntentConstant;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.model.constant.SharedPreferencesConstant;
import com.app.model.constant.SharedPreferencesItemConstant;
import com.app.register.RegisterActivity;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.response.LoginResponse;
import com.app.retrofit.utils.ResponseUtils;
import com.app.utils.CallbackEntity;
import com.app.utils.LanguageUtils;
import com.app.utils.SharedPreferencesUtils;
import com.app.utils.ViewUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.img_logo)
    ImageView imgLogo;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.cb_remember_me)
    CheckBox cbRememberMe;

    @OnClick(R.id.btn_login)
    public void btnLoginClick() {
        String username = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        doLogin(username, password, false);
    }

    @OnClick(R.id.btn_register)
    public void btnRegisterClick() {
        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btn_flag_france)
    public void selectFranceLanguage() {
        LanguageUtils.changeLanguage(getBaseContext(), LanguageUtils.FRANCE);
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);
    }

    @OnClick(R.id.btn_flag_usa)
    public void selectEnglishLanguage() {
        LanguageUtils.changeLanguage(getBaseContext(), LanguageUtils.ENGLISH);
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);
    }

    @OnClick(R.id.tv_remember_me)
    public void tvRememberMeClick() {
        cbRememberMe.setChecked(!cbRememberMe.isChecked());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguageUtils.loadPrefLanguage(getBaseContext());
        setContentView(R.layout.login);

        ButterKnife.bind(this);
        loadAnimation();

        if (getIntent().getExtras() != null) {
            handleExtras();
        }

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SharedPreferencesConstant.LOGIN, MODE_PRIVATE);
        if (sharedPreferences.contains(SharedPreferencesItemConstant.EMAIL) &&
                sharedPreferences.contains(SharedPreferencesItemConstant.PASSWORD)) {
            String emailRestore = sharedPreferences.getString(SharedPreferencesItemConstant.EMAIL, "");
            String passwordRestore = sharedPreferences.getString(SharedPreferencesItemConstant.PASSWORD, "");
            doLogin(emailRestore, passwordRestore, true);
        }
    }

    private void loadAnimation() {
        Animation shakeAnimation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.shake);
        imgLogo.setOnClickListener(v -> imgLogo.startAnimation(shakeAnimation));
    }

    private void handleExtras() {
        if (getIntent().getBooleanExtra(IntentConstant.INTENT_DELETE_CREDENTIALS, false)) {
            SharedPreferencesUtils.deleteCredentials(getApplicationContext());
        }
        String email_prefill = getIntent().getStringExtra(IntentConstant.EMAIL_PREFILL);
        if (email_prefill != null) {
            etEmail.setText(email_prefill);
        }
    }

    private void doLogin(String email, String password, boolean autoLogin) {
        DialogHelper.getInstance().showLoading(this);
        Call<LoginResponse> call = RetrofitInstance.getAPIService().doLogin(email, password);
        call.enqueue(new CallbackEntity<>(LoginResponse.class, new CallbackEntity.CallbackEntityResponse<LoginResponse>() {
            @Override
            public void onSuccess(LoginResponse loginResponse) {
                DialogHelper.getInstance().dismissDialog();

                if (loginResponse.getCode_response().equals(ResponseCodeHttpConstant.SUCCESS)) {
                    if (cbRememberMe.isChecked()) {
                        SharedPreferencesUtils.uploadCredentials(getApplicationContext(), email, password);
                    }

                    loginResponse.getEntity().setRole(EnumRole.UNKNOWN);
                    UserInstance.initializeUser(loginResponse.getEntity());

                    RetrofitInstance.loadBuilderRetrofit();

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                    finish();
                } else if (loginResponse.getCode_response().equals(ResponseCodeHttpConstant.UNAUTHORIZED)) {
                    if (autoLogin) {
                        SharedPreferencesUtils.deleteCredentials(getApplicationContext());
                    }
                    ViewUtils.toast(getApplicationContext(), R.string.authentification_failed, true);
                }
            }

            @Override
            public void onFailure() {
                if (autoLogin) {
                    SharedPreferencesUtils.deleteCredentials(getApplicationContext());
                }
                DialogHelper.getInstance().dismissDialog();
                ResponseUtils.onCallFailed(getApplicationContext());
            }
        }, ResponseCodeHttpConstant.SUCCESS, ResponseCodeHttpConstant.UNAUTHORIZED));
    }
}
