package com.app.model;

import com.app.retrofit.model.object.User;

public class UserInstance {
    private static User user;

    public static void initializeUser(User user) {
        UserInstance.user = user;
    }

    public static User getUser() {
        return user;
    }
}
