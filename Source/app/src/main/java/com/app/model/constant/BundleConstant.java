package com.app.model.constant;

public class BundleConstant {
    public static final String BUNDLE_ITEM = "0";
    public static final String BUNDLE_LOCATION = "1";
    public static final String BUNDLE_ADD_MODE = "2";
    public static final String BUNDLE_UPDATE = "3";
    public static final String BUNDLE_EXEMPLARY = "4";
}
