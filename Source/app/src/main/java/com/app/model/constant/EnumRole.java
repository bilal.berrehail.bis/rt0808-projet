package com.app.model.constant;

public enum EnumRole {
    UNKNOWN(0, "unknown", false, false, false),
    WORKER(1, "worker", false, false, false),
    ADMIN(2, "admin", true, true, true);

    private int id;
    private String label;
    private boolean viewUsersLibrary;
    private boolean seeTokenLibrary;
    private boolean modifyLibrary;

    EnumRole(int id, String label, boolean viewUsersLibrary, boolean seeTokenLibrary, boolean modifyLibrary) {
        this.id = id;
        this.label = label;
        this.viewUsersLibrary = viewUsersLibrary;
        this.seeTokenLibrary = seeTokenLibrary;
        this.modifyLibrary = modifyLibrary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isViewUsersLibrary() {
        return viewUsersLibrary;
    }

    public void setViewUsersLibrary(boolean viewUsersLibrary) {
        this.viewUsersLibrary = viewUsersLibrary;
    }

    public boolean isSeeTokenLibrary() {
        return seeTokenLibrary;
    }

    public void setSeeTokenLibrary(boolean seeTokenLibrary) {
        this.seeTokenLibrary = seeTokenLibrary;
    }

    public boolean isModifyLibrary() {
        return modifyLibrary;
    }

    public void setModifyLibrary(boolean modifyLibrary) {
        this.modifyLibrary = modifyLibrary;
    }

    public static EnumRole getEnumRoleFromRole(String role) {
        for (EnumRole enumRole : values()) {
            if (enumRole.getLabel().equals(role)) {
                return enumRole;
            }
        }
        return null;
    }
}
