package com.app.model.constant;

public class IntentConstant {
    public static final String INTENT_DELETE_CREDENTIALS = "INTENT_DELETE_CREDENTIALS";
    public static final String EMAIL_PREFILL = "EMAIL_PREFILL";
}
