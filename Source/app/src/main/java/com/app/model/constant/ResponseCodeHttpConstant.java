package com.app.model.constant;

public class ResponseCodeHttpConstant {
    public static final Integer SUCCESS = 200;
    public static final Integer CREATE_SUCCESS = 201;
    public static final Integer BAD_REQUEST = 400;
    public static final Integer METHOD_NOT_ALLOWED = 405;
    public static final Integer CONFLIT = 409;
    public static final Integer UNAUTHORIZED = 401;
    public static final Integer FORBIDDEN = 403;
    public static final Integer INTERNAL_SERVER_ERROR = 500;
}
