package com.app.model.constant;

public class ScanConstant {
    public static final String ISBN_FORMAT = "CODE_128";
    public static final String EXEMPLARY_FORMAT = "QR_CODE";
}
