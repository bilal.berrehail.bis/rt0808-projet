package com.app.model.constant;

public class SharedPreferencesConstant {
    public static final String LOGIN = "LOGIN";
    public static final String LANGUAGE = "LANGUAGE";
}
