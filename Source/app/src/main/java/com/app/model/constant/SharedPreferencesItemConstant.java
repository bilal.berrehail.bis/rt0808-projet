package com.app.model.constant;

public class SharedPreferencesItemConstant {
    public static final String EMAIL = "EMAIL";
    public static final String PASSWORD = "PASSWORD";
    public static final String PREF_LANGUAGE = "PREF_LANGUAGE";
}
