package com.app.model.decoration;

import android.graphics.Rect;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

public class MarginItemDecoration extends RecyclerView.ItemDecoration {

    private final int mItemOffset;

    public MarginItemDecoration(int mItemOffset) {
        this.mItemOffset = mItemOffset;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, RecyclerView parent, @NonNull RecyclerView.State state) {
        if (parent.getLayoutManager() == null) return;

        if (((LinearLayoutManager) parent.getLayoutManager()).getOrientation() == RecyclerView.VERTICAL) {
            if (parent.getChildAdapterPosition(view) == 0) {
                outRect.top = mItemOffset;
            }
            outRect.left = mItemOffset;
            outRect.right = mItemOffset;
            outRect.bottom = mItemOffset;
        } else {
            if (parent.getChildAdapterPosition(view) == 0) {
                outRect.left = mItemOffset;
            }
            outRect.top = mItemOffset;
            outRect.right = mItemOffset;
            outRect.bottom = mItemOffset;
        }
    }
}
