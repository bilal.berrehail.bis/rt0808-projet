package com.app.model.fields;

import com.app.retrofit.model.object.Category;

import java.util.HashMap;

public class CategoryFields {
    public static final String id = "_id";
    public static final String label = "label";

    public static HashMap<String, Object> serializeForCreation(Category category) {
        HashMap<String, Object> mapCategory = new HashMap<>();

        if (category.getLabel() != null) {
            mapCategory.put(CategoryFields.label, category.getLabel());
        }

        return mapCategory;
    }

    public static HashMap<String, Object> serializeForUpdating(Category category) {
        HashMap<String, Object> mapCategory = new HashMap<>();

        if (category.getLabel() != null) {
            mapCategory.put(CategoryFields.label, category.getLabel());
        }

        return mapCategory;
    }
}
