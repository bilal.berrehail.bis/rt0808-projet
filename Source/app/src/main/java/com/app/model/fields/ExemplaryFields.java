package com.app.model.fields;

import com.app.retrofit.model.object.Exemplary;

import java.util.HashMap;

public class ExemplaryFields {
    public static final String id = "_id";
    public static final String label = "label";
    public static final String location = "location";
    public static final String item = "item";
    public static final String lend_information = "lend_information";
    public static final String created = "created";

    public static HashMap<String, Object> serializeForCreation(Exemplary exemplary) {
        HashMap<String, Object> mapExemplary = new HashMap<>();

        if (exemplary.getLabel() != null) {
            mapExemplary.put(ExemplaryFields.label, exemplary.getLabel());
        }

        return mapExemplary;
    }

    public static HashMap<String, Object> serializeForUpdating(Exemplary exemplary) {
        HashMap<String, Object> mapExemplary = new HashMap<>();

        if (exemplary.getLabel() != null) {
            mapExemplary.put(ExemplaryFields.label, exemplary.getLabel());
        }

        return mapExemplary;
    }
}