package com.app.model.fields;

import com.app.retrofit.model.object.Item;

import java.util.HashMap;

public class ItemFields {
    public static final String id = "_id";
    public static final String label = "label";
    public static final String author = "author";
    public static final String editor = "editor";
    public static final String type = "type";
    public static final String category = "category";
    public static final String date_of_publish = "date_of_publish";
    public static final String isbn = "isbn";
    public static final String path_img = "path_img";
    public static final String total_exemplaries = "total_exemplaries";

    public static HashMap<String, Object> serializeForCreation(Item item) {
        HashMap<String, Object> mapItem = new HashMap<>();

        if (item.getLabel() != null) {
            mapItem.put(ItemFields.label, item.getLabel());
        }
        if (item.getAuthor() != null) {
            mapItem.put(ItemFields.author, item.getAuthor());
        }
        if (item.getEditor() != null) {
            mapItem.put(ItemFields.editor, item.getEditor());
        }
        if (item.getType() != null) {
            mapItem.put(ItemFields.type, item.getType());
        }
        if (item.getCategory() != null && item.getCategory().getId() != null) {
            mapItem.put(ItemFields.category, item.getCategory().getId());
        }
        if (item.getDate_of_publish() != null) {
            mapItem.put(ItemFields.date_of_publish, item.getDate_of_publish());
        }
        if (item.getIsbn() != null) {
            mapItem.put(ItemFields.isbn, item.getIsbn());
        }
        if (item.getPath_img() != null) {
            mapItem.put(ItemFields.path_img, item.getPath_img());
        }

        return mapItem;
    }

    public static HashMap<String, Object> serializeForUpdating(Item item) {
        HashMap<String, Object> mapItem = new HashMap<>();

        if (item.getLabel() != null) {
            mapItem.put(ItemFields.label, item.getLabel());
        }
        if (item.getAuthor() != null) {
            mapItem.put(ItemFields.author, item.getAuthor());
        }
        if (item.getEditor() != null) {
            mapItem.put(ItemFields.editor, item.getEditor());
        }
        if (item.getType() != null) {
            mapItem.put(ItemFields.type, item.getType());
        }
        if (item.getCategory() != null && item.getCategory().getId() != null) {
            mapItem.put(ItemFields.category, item.getCategory().getId());
        } else {
            mapItem.put(ItemFields.category, "");
        }
        if (item.getDate_of_publish() != null) {
            mapItem.put(ItemFields.date_of_publish, item.getDate_of_publish());
        }
        if (item.getIsbn() != null) {
            mapItem.put(ItemFields.isbn, item.getIsbn());
        }
        if (item.getPath_img() != null) {
            mapItem.put(ItemFields.path_img, item.getPath_img());
        }

        return mapItem;
    }
}