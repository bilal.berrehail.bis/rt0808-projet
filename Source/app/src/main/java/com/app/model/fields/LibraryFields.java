package com.app.model.fields;

import com.app.retrofit.model.object.Library;

import java.util.HashMap;

public class LibraryFields {
    public static final String id = "_id";
    public static final String name = "name";
    public static final String address = "address";
    public static final String path_img = "path_img";

    public static HashMap<String, Object> serializeForCreation(Library library) {
        HashMap<String, Object> mapLibrary = new HashMap<>();

        if (library.getName() != null) {
            mapLibrary.put(LibraryFields.name, library.getName());
        }
        if (library.getAddress() != null) {
            mapLibrary.put(LibraryFields.address, library.getAddress());
        }
        if (library.getPath_img() != null) {
            mapLibrary.put(LibraryFields.path_img, library.getPath_img());
        }

        return mapLibrary;
    }

    public static HashMap<String, Object> serializeForUpdating(Library library) {
        HashMap<String, Object> mapLibrary = new HashMap<>();

        if (library.getName() != null) {
            mapLibrary.put(LibraryFields.name, library.getName());
        }
        if (library.getAddress() != null) {
            mapLibrary.put(LibraryFields.address, library.getAddress());
        }
        if (library.getPath_img() != null) {
            mapLibrary.put(LibraryFields.path_img, library.getPath_img());
        }

        return mapLibrary;
    }
}
