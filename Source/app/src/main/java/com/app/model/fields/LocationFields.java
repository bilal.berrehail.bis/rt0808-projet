package com.app.model.fields;

import com.app.retrofit.model.object.Location;

import java.util.HashMap;

public class LocationFields {
    public static final String id = "_id";
    public static final String label = "label";
    public static final String quantity_max = "quantity_max";
    public static final String total_exemplaries = "total_exemplaries";
    public static final String total_items = "total_items";

    public static HashMap<String, Object> serializeForCreation(Location location) {
        HashMap<String, Object> mapLocation = new HashMap<>();

        if (location.getLabel() != null) {
            mapLocation.put(CategoryFields.label, location.getLabel());
        }

        return mapLocation;
    }

    public static HashMap<String, Object> serializeForUpdating(Location location) {
        HashMap<String, Object> mapLocation = new HashMap<>();

        if (location.getLabel() != null) {
            mapLocation.put(CategoryFields.label, location.getLabel());
        }

        return mapLocation;
    }
}