package com.app.model.fields;

import com.app.retrofit.model.object.User;

import java.util.HashMap;

public class UserFields {
    public static final String id = "_id";
    public static final String first_name = "first_name";
    public static final String last_name = "last_name";
    public static final String email = "email";
    public static final String password = "password";
    public static final String token = "token";

    public static HashMap<String, Object> serializeForCreation(User user) {
        HashMap<String, Object> mapUser = new HashMap<>();

        if (user.getFirst_name() != null) {
            mapUser.put(UserFields.first_name, user.getFirst_name());
        }

        if (user.getLast_name() != null) {
            mapUser.put(UserFields.last_name, user.getLast_name());
        }

        if (user.getEmail() != null) {
            mapUser.put(UserFields.email, user.getEmail());
        }

        if (user.getPassword() != null) {
            mapUser.put(UserFields.password, user.getPassword());
        }

        return mapUser;
    }
}
