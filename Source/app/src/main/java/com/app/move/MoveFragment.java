package com.app.move;

import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.R;
import com.app.base.BaseFragment;
import com.app.databinding.FragmentMoveBinding;
import com.app.dialog.PopupConfirmCancel;
import com.app.dialog.helper.DialogHelper;
import com.app.exemplary.adapter.ExemplaryItemListingAdapter;
import com.app.exemplary.holder.ExemplaryListingHolder;
import com.app.home.HomeActivity;
import com.app.item.fragment.ItemListingFragment;
import com.app.location.adapter.LocationListingAdapter;
import com.app.location.holder.LocationListingHolder;
import com.app.model.constant.BundleConstant;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Exemplary;
import com.app.retrofit.model.object.Item;
import com.app.retrofit.model.object.Location;
import com.app.retrofit.model.response.MoveExemplaryResponse;
import com.app.scanner.PopupScan;
import com.app.utils.BundleUtils;
import com.app.utils.CallbackEntity;
import com.app.utils.DataScanUtils;
import com.app.utils.ViewUtils;
import com.app.viewmodel.MoveViewModel;
import com.google.gson.Gson;

import java.util.Objects;

import butterknife.BindView;
import retrofit2.Call;

public class MoveFragment extends BaseFragment implements Toolbar.OnMenuItemClickListener, ItemListingFragment.RemoteUseListener {
    @BindView(R.id.fl_child_fragment)
    FrameLayout flChildFragment;
    @BindView(R.id.iv_move)
    ImageView ivMove;
    @BindView(R.id.cl_select_item)
    ConstraintLayout clSelectItem;
    @BindView(R.id.btn_select_item)
    Button btnSelectItem;
    @BindView(R.id.btn_select_exemplary)
    Button btnSelectExemplary;
    @BindView(R.id.iv_edit_location_arrival)
    ImageView ivEditLocationArrival;

    private MoveViewModel moveViewModel;

    private PopupWindow popupSelectExemplary;
    private ExemplaryItemListingAdapter exemplaryItemListingAdapter;

    private PopupScan popupScan;
    private PopupWindow popupSelectLocation;

    @Override
    protected int getLayout() {
        return R.layout.fragment_move;
    }

    @Override
    protected int getTitle() {
        return R.string.do_move;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        homeActivity.inflateToolbarMenu(R.menu.menu_move, this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentMoveBinding fragmentMoveBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_move, container, false);
        View root = fragmentMoveBinding.getRoot();
        moveViewModel = new ViewModelProvider(this).get(MoveViewModel.class);
        fragmentMoveBinding.setMove(moveViewModel);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Animation shake = AnimationUtils.loadAnimation(homeActivity, R.anim.shake);
        ivMove.animate().translationX((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics())).setDuration(600);
        ivMove.setOnClickListener(v -> ivMove.startAnimation(shake));

        btnSelectItem.setOnClickListener(v -> showItemListing());
        btnSelectExemplary.setOnClickListener(v -> showExemplaries());
        ivEditLocationArrival.setOnClickListener(v -> {
            if (popupSelectLocation != null) {
                popupSelectLocation.showAsDropDown(clSelectItem);
                ViewUtils.hideKeyboard(homeActivity);
            }
        });

        Object exemplaryJsonRetrieved = BundleUtils.getValue(getArguments(), BundleConstant.BUNDLE_EXEMPLARY);
        if (exemplaryJsonRetrieved != null) {
            Exemplary exemplaryRetrieved = new Gson().fromJson((String) exemplaryJsonRetrieved, Exemplary.class);
            setItemOnVM(exemplaryRetrieved.getItem());
            setExemplaryOnVM(exemplaryRetrieved);
        }
    }

    @Override
    public void onViewCompleteLoaded() {
        super.onViewCompleteLoaded();
        initLocationAdapter();
    }

    @Override
    protected void afterActivityCreated() {
    }

    private void initLocationAdapter() {
        LocationListingAdapter locationListingAdapter = new LocationListingAdapter(homeActivity, null, false, false, false, new LocationListingHolder.LocationListingListener() {
            @Override
            public void onClick(Location location) {
                moveViewModel.setLocationArrival(location);
                if (popupSelectLocation != null) {
                    popupSelectLocation.dismiss();
                }
            }

            @Override
            public void onDelete(Location location) {
            }

            @Override
            public void onConsultationExemplarys(Location locatNion) {
            }
        });

        popupSelectLocation = ViewUtils.createPopupMenu(homeActivity, clSelectItem, locationListingAdapter, new LinearLayoutManager(homeActivity));
    }

    private void initExemplaryItemAdapter(Item item) {
        if (item != null && item.getId() != null) {
            exemplaryItemListingAdapter = new ExemplaryItemListingAdapter(homeActivity, item.getId(), null, false, false, new ExemplaryListingHolder.ExemplaryListingListener() {
                @Override
                public void onClick(Exemplary exemplary) {
                    popupSelectExemplary.dismiss();
                    setExemplaryOnVM(exemplary);
                }

                @Override
                public void onLend(Exemplary exemplary) {

                }

                @Override
                public void onMoveRequest(Exemplary exemplary) {
                }
            });

            popupSelectExemplary = ViewUtils.createPopupMenu(homeActivity, clSelectItem, exemplaryItemListingAdapter, new LinearLayoutManager(homeActivity));
        }
    }

    private void showExemplaries() {
        if (popupSelectExemplary != null) {
            if (moveViewModel.getItem() != null && moveViewModel.getItem().get() != null && Objects.requireNonNull(moveViewModel.getItem().get()).getId() != null) {
                if (exemplaryItemListingAdapter.getItemCount() == 0) {
                    ViewUtils.toast(homeActivity, R.string.no_exemplary_found, true);
                } else {
                    popupSelectExemplary.showAsDropDown(clSelectItem);
                    ViewUtils.hideKeyboard(homeActivity);
                }
            } else {
                ViewUtils.toast(homeActivity, R.string.select_item, true);
            }
        } else {
            ViewUtils.toast(homeActivity, R.string.select_item, true);
        }
    }

    private void showItemListing() {
        flChildFragment.setVisibility(View.VISIBLE);

        ItemListingFragment itemListingFragment = new ItemListingFragment();

        Bundle b = new Bundle();
        b.putBoolean(BundleConstant.BUNDLE_ADD_MODE, false);

        itemListingFragment.setArguments(b);

        getChildFragmentManager().beginTransaction().add(R.id.fl_child_fragment, itemListingFragment).commit();
    }

    private void setItemOnVM(Item item) {
        if (item == null) return;

        moveViewModel.setItem(item);
        initExemplaryItemAdapter(item);

        if (moveViewModel.getExemplary().get() != null) {
            if (Objects.requireNonNull(moveViewModel.getExemplary().get()).getItem() != null &&
                    Objects.requireNonNull(moveViewModel.getExemplary().get()).getItem().getId() != null) {
                if (!Objects.requireNonNull(moveViewModel.getExemplary().get()).getItem().getId().equals(item.getId())) {
                    moveViewModel.setExemplary(null);
                }
            }
        }
    }

    private void setExemplaryOnVM(Exemplary exemplary) {
        if (exemplary == null) return;

        if (exemplary.getItem() != null && exemplary.getItem().getId() != null) {
            if (moveViewModel.getItem().get() != null && Objects.requireNonNull(moveViewModel.getItem().get()).getId() != null) {
                if (!Objects.requireNonNull(moveViewModel.getItem().get()).getId().equals(exemplary.getItem().getId())) {
                    return;
                }
            }
        }

        moveViewModel.setExemplary(exemplary);
    }

    private void handleScanExemplary() {
        popupScan = PopupScan.getInstance(homeActivity, result -> {
            DataScanUtils.buildExemplary(result.getText(), new DataScanUtils.DataScanUtilsListener() {
                @Override
                public void onBuildExemplary(Exemplary exemplary) {
                    if (exemplary.getItem() != null) {
                        setItemOnVM(exemplary.getItem());
                        setExemplaryOnVM(exemplary);
                    }
                }

                @Override
                public void onBuildFailed() {
                    ViewUtils.toast(homeActivity, R.string.no_exemplary_found, true);
                }
            });
            popupScan.dismiss();
        });
        popupScan.show();
    }

    private void eraseChildFragment() {
        flChildFragment.setVisibility(View.GONE);
        for (Fragment childFragment : getChildFragmentManager().getFragments()) {
            getChildFragmentManager().beginTransaction().remove(childFragment).commit();
        }
        homeActivity.setToolbarTitle(getTitle());
    }

    private void onValidate() {
        if (moveViewModel.getItem().get() == null) {
            ViewUtils.toast(homeActivity, R.string.select_item, true);
            return;
        }
        if (moveViewModel.getExemplary().get() == null) {
            ViewUtils.toast(homeActivity, R.string.select_exemplary, true);
            return;
        }
        if (moveViewModel.getLocationArrival().get() == null) {
            ViewUtils.toast(homeActivity, R.string.select_arrival_location, true);
            return;
        }
        PopupConfirmCancel popupConfirmCancel = PopupConfirmCancel.getInstance(homeActivity, R.string.confirm_move, this::onValidateMove);
        popupConfirmCancel.show();
    }

    private void onValidateMove() {
        DialogHelper.getInstance().showLoading(homeActivity);

        Call<MoveExemplaryResponse> call = RetrofitInstance.getAPIService().moveExemplary(Objects.requireNonNull(moveViewModel.getExemplary().get()).getId(), Objects.requireNonNull(moveViewModel.getLocationArrival().get()).getId());
        call.enqueue(new CallbackEntity<>(MoveExemplaryResponse.class, new CallbackEntity.CallbackEntityResponse<MoveExemplaryResponse>() {
            @Override
            public void onSuccess(MoveExemplaryResponse moveExemplaryResponse) {
                DialogHelper.getInstance().dismissDialog();
                if (moveExemplaryResponse != null) {
                    if (moveExemplaryResponse.getCode_response().equals(ResponseCodeHttpConstant.SUCCESS)) {
                        ViewUtils.toast(homeActivity, R.string.move_successful, false);
                        closeFragmentWithAnimation();
                    } else if (moveExemplaryResponse.getCode_response().equals(ResponseCodeHttpConstant.CONFLIT)) {
                        ViewUtils.toast(homeActivity, R.string.target_same_location, true);
                    } else if (moveExemplaryResponse.getCode_response().equals(ResponseCodeHttpConstant.BAD_REQUEST)) {
                        ViewUtils.toast(homeActivity, R.string.move_failed, true);
                    }
                }
            }

            @Override
            public void onFailure() {
                DialogHelper.getInstance().dismissDialog();
                ViewUtils.toast(homeActivity, R.string.move_failed, true);
            }
        }, ResponseCodeHttpConstant.SUCCESS, ResponseCodeHttpConstant.BAD_REQUEST, ResponseCodeHttpConstant.CONFLIT));
    }

    private void closeFragmentWithAnimation() {
        ivMove.animate().translationX((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 340, getResources().getDisplayMetrics())).setDuration(350);

        Bundle bundleUpdate = new Bundle();
        bundleUpdate.putBoolean(BundleConstant.BUNDLE_UPDATE, true);
        homeActivity.remoteCallPreviousFragment(bundleUpdate);
        final Runnable r = () -> homeActivity.popBackStack();

        new Handler().postDelayed(r, 350);
    }

    @Override
    public boolean onBackPressed() {
        if (getChildFragmentManager().getFragments().size() > 0) {
            eraseChildFragment();
            return false;
        }
        return true;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_validate:
                onValidate();
                return true;
            case R.id.action_scan:
                handleScanExemplary();
                break;
        }
        return false;
    }

    @Override
    public void onItemClick(Item item) {
        eraseChildFragment();
        setItemOnVM(item);
    }
}
