package com.app.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.app.R;
import com.app.databinding.RegisterBinding;
import com.app.dialog.helper.DialogHelper;
import com.app.login.LoginActivity;
import com.app.model.constant.IntentConstant;
import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.model.fields.UserFields;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.User;
import com.app.retrofit.model.response.RegisterResponse;
import com.app.retrofit.utils.ResponseUtils;
import com.app.utils.CallbackEntity;
import com.app.utils.ViewUtils;

import java.util.HashMap;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class RegisterActivity extends AppCompatActivity {
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;

    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RegisterBinding registerBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.register, null, false);
        View root = registerBinding.getRoot();

        this.user = new User();
        registerBinding.setUser(user);

        setContentView(root);

        ButterKnife.bind(this, root);
    }

    @OnClick(R.id.btn_back)
    public void backToLogin() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btn_register)
    public void doRegister() {
        if (user.getPassword() == null || !etConfirmPassword.getText().toString().equals(user.getPassword())) {
            ViewUtils.toast(getApplicationContext(), R.string.password_not_same, true);
            return;
        }
        if (user.getEmail() == null || !Pattern.matches("[a-zA-Z0-9_.]+[@][a-z]+[.][a-z]{2,3}", user.getEmail())) {
            ViewUtils.toast(getApplicationContext(), R.string.email_not_correct, true);
            return;
        }

        HashMap<String, Object> mapUser = UserFields.serializeForCreation(user);

        DialogHelper.getInstance().showLoading(this);
        Call<RegisterResponse> call = RetrofitInstance.getAPIService().doRegister(mapUser);
        call.enqueue(new CallbackEntity<>(RegisterResponse.class, new CallbackEntity.CallbackEntityResponse<RegisterResponse>() {
            @Override
            public void onSuccess(RegisterResponse registerResponse) {
                DialogHelper.getInstance().dismissDialog();
                if (registerResponse.getCode_response().equals(ResponseCodeHttpConstant.SUCCESS)) {
                    ViewUtils.toast(getApplicationContext(), R.string.register_successful, false);

                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.putExtra(IntentConstant.EMAIL_PREFILL, user.getEmail());
                    startActivity(intent);
                    finish();
                } else if (registerResponse.getCode_response().equals(ResponseCodeHttpConstant.INTERNAL_SERVER_ERROR)) {
                    ViewUtils.toast(getApplicationContext(), R.string.register_failed, true);
                }
            }

            @Override
            public void onFailure() {
                DialogHelper.getInstance().dismissDialog();
                ResponseUtils.onCallFailed(getApplicationContext());
            }
        }, ResponseCodeHttpConstant.SUCCESS, ResponseCodeHttpConstant.INTERNAL_SERVER_ERROR));
    }
}
