package com.app.retrofit.instance;

import com.app.model.UserInstance;
import com.app.retrofit.service.APIService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {
    private static Retrofit retrofitWoutToken;
    private static Retrofit retrofit;
    private static final String BASE_URL = "https://api-library-ter.herokuapp.com/";

    public static void loadBuilderRetrofit() {
        retrofit = retrofit.newBuilder().client(provideHttpClient(true).build()).build();
    }

    public static void loadBuilderRetrofitWoutToken() {
        retrofitWoutToken = retrofit.newBuilder().client(provideHttpClient(false).build()).build();
    }

    private static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            // Pour JSON Malformé -> à corriger
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            loadBuilderRetrofit();
        }

        return retrofit;
    }

    private static Retrofit getRetrofitWoutTokenInstance() {
        if (retrofitWoutToken == null) {
            // Pour JSON Malformé -> à corriger
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofitWoutToken = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            loadBuilderRetrofitWoutToken();
        }

        return retrofitWoutToken;
    }

    private static OkHttpClient.Builder provideHttpClient(boolean wToken) {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        okHttpClient.addInterceptor(logging);

        okHttpClient.addInterceptor(chain -> {

            Request original = chain.request();

            Request.Builder builder = original.newBuilder();
            if (wToken) {
                if (UserInstance.getUser() != null && UserInstance.getUser().getToken() != null) {
                    builder = builder.header("Authorization", "Bearer " + UserInstance.getUser().getToken());
                }
            }

            return chain.proceed(builder.build());

        });

        okHttpClient.connectTimeout(15000, TimeUnit.MILLISECONDS);
        okHttpClient.writeTimeout(60, TimeUnit.SECONDS);
        okHttpClient.readTimeout(60, TimeUnit.SECONDS);

        return okHttpClient;
    }

    public static APIService getAPIService() {
        return getRetrofitInstance().create(APIService.class);
    }

    public static APIService getAPIServiceWithoutToken() {
        return getRetrofitWoutTokenInstance().create(APIService.class);
    }
}
