package com.app.retrofit.model.object;

import com.google.gson.annotations.SerializedName;

public class Category extends Entity {
    @SerializedName("_id")
    private String id;
    private String label;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
