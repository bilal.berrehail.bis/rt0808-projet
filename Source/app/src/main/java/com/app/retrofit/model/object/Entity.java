package com.app.retrofit.model.object;

import androidx.annotation.NonNull;
import androidx.databinding.BaseObservable;

import java.io.Serializable;

public class Entity extends BaseObservable implements Serializable, Cloneable {
    @NonNull
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
