package com.app.retrofit.model.object;

import androidx.databinding.Bindable;

import com.app.BR;
import com.app.utils.DateFormat;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Exemplary extends Entity {
    @SerializedName("_id")
    private String id;
    private String label;
    private Location location;
    private Item item;
    private LendInformation lend_information;
    private Date created;
    private Date modified;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Bindable
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
        notifyPropertyChanged(BR.location);
    }

    @Bindable
    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
        notifyPropertyChanged(BR.item);
    }

    @Bindable
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
        notifyPropertyChanged(BR.label);
    }

    @Bindable
    public LendInformation getLendInformation() {
        return lend_information;
    }

    public void setLendInformation(LendInformation lend_information) {
        this.lend_information = lend_information;
        notifyPropertyChanged(BR.lendInformation);
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
        notifyPropertyChanged(BR.createdFormatted);
    }

    @Bindable
    public String getCreatedFormatted() {
        return DateFormat.formatSimpleFormat(created);
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
