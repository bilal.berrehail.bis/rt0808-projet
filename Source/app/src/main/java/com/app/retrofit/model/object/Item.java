package com.app.retrofit.model.object;

import androidx.databinding.Bindable;

import com.app.BR;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Item extends Entity {
    @SerializedName("_id")
    private String id;
    private String label;
    private String author;
    private String editor;
    private String type;
    private Category category;
    private String date_of_publish;
    private String isbn;
    private String path_img;
    private Integer total_exemplaries;
    private Date created;
    private Date modified;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Bindable
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
        notifyPropertyChanged(BR.label);
    }

    @Bindable
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
        notifyPropertyChanged(BR.author);
    }

    @Bindable
    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
        notifyPropertyChanged(BR.editor);
    }

    @Bindable
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        notifyPropertyChanged(BR.type);
    }

    @Bindable
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
        notifyPropertyChanged(BR.category);
    }

    @Bindable
    public String getDate_of_publish() {
        return date_of_publish;
    }

    public void setDate_of_publish(String date_of_publish) {
        this.date_of_publish = date_of_publish;
        notifyPropertyChanged(BR.date_of_publish);
    }

    @Bindable
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
        notifyPropertyChanged(BR.isbn);
    }

    @Bindable
    public String getPath_img() {
        return path_img;
    }

    public void setPath_img(String path_img) {
        this.path_img = path_img;
        notifyPropertyChanged(BR.path_img);
    }

    @Bindable
    public Integer getTotal_exemplaries() {
        return total_exemplaries;
    }

    public void setTotal_exemplaries(Integer total_exemplaries) {
        this.total_exemplaries = total_exemplaries;
        notifyPropertyChanged(BR.total_exemplaries);
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
