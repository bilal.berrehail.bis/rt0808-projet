package com.app.retrofit.model.object;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.app.BR;
import com.app.utils.DateFormat;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class LendInformation extends BaseObservable {
    private String receiver_name;
    @SerializedName("date")
    private Long date_limit;
    private String note;

    @Bindable
    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String name) {
        this.receiver_name = name;
        notifyPropertyChanged(BR.receiver_name);
    }

    public Long getDate_limit() {
        return date_limit;
    }

    public void setDate_limit(Long date_limit) {
        this.date_limit = date_limit;
        notifyPropertyChanged(BR.date_limitFormatted);
    }

    @Bindable
    public String getDate_limitFormatted() {
        if (date_limit != null) {
            return DateFormat.formatSimpleFormat(new Date(date_limit));
        }
        return "-";
    }

    @Bindable
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
        notifyPropertyChanged(BR.note);
    }
}
