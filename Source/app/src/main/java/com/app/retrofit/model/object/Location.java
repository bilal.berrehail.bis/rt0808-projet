package com.app.retrofit.model.object;

import androidx.databinding.Bindable;

import com.app.BR;
import com.google.gson.annotations.SerializedName;

public class Location extends Entity {
    @SerializedName("_id")
    private String id;
    private String label;
    private Integer quantity_max;
    private Integer total_exemplaries;
    private Integer total_items;

    @Bindable
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    @Bindable
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
        notifyPropertyChanged(BR.label);
    }

    @Bindable
    public Integer getQuantity_max() {
        return quantity_max;
    }

    public void setQuantity_max(Integer quantity_max) {
        this.quantity_max = quantity_max;
        notifyPropertyChanged(BR.quantity_max);
    }

    @Bindable
    public Integer getTotal_exemplaries() {
        return total_exemplaries;
    }

    public void setTotal_exemplaries(Integer total_exemplaries) {
        this.total_exemplaries = total_exemplaries;
        notifyPropertyChanged(BR.total_exemplaries);
    }

    @Bindable
    public Integer getTotal_items() {
        return total_items;
    }

    public void setTotal_items(Integer total_items) {
        this.total_items = total_items;
        notifyPropertyChanged(BR.total_items);
    }
}
