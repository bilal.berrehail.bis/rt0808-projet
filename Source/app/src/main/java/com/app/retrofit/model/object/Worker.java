package com.app.retrofit.model.object;

import com.google.gson.annotations.SerializedName;

public class Worker extends Entity {
    @SerializedName("_id")
    private String id;
    private String role;
    @SerializedName("worker")
    private User user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
