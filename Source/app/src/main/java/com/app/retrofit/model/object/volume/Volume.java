package com.app.retrofit.model.object.volume;

import com.app.retrofit.model.object.Entity;

public class Volume extends Entity {
    private VolumeInfo volumeInfo;

    public VolumeInfo getVolumeInfo() {
        return volumeInfo;
    }

    public void setVolumeInfo(VolumeInfo volumeInfo) {
        this.volumeInfo = volumeInfo;
    }
}

