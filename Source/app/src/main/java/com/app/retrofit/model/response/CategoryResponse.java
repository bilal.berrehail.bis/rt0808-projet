package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Category;
import com.google.gson.annotations.SerializedName;

public class CategoryResponse extends RetrofitResponse<Category> {
    private String msg;

    private String id_category;

    @SerializedName(value = "category", alternate = "category_created")
    private Category category;

    public String getMsg() {
        return msg;
    }

    public String getId_category() {
        return id_category;
    }

    @Override
    public Category getEntity() {
        return category;
    }
}