package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Category;

public class DeleteCategoryResponse extends RetrofitResponse<Category> {
    private String msg;
    private Integer total_deleted;
    private Integer item_updated;

    public String getMsg() {
        return msg;
    }

    public Integer getTotal_deleted() {
        return total_deleted;
    }

    public Integer getItem_updated() {
        return item_updated;
    }
}
