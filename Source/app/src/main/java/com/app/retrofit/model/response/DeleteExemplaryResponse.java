package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Exemplary;

public class DeleteExemplaryResponse extends RetrofitResponse<Exemplary> {
    private String msg;

    public String getMsg() {
        return msg;
    }
}
