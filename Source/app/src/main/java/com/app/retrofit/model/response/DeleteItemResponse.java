package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Item;

public class DeleteItemResponse extends RetrofitResponse<Item> {
    private String msg;
    private String item_id;
    private Integer total_deleted;
    private Integer total_exemplary_related;

    public String getMsg() {
        return msg;
    }

    public String getItem_id() {
        return item_id;
    }

    public Integer getTotal_deleted() {
        return total_deleted;
    }

    public Integer getTotal_exemplary_related() {
        return total_exemplary_related;
    }
}
