package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Location;

public class DeleteLocationResponse extends RetrofitResponse<Location> {
    private String msg;
    private String location_id;
    private Integer total_deleted;
    private Integer total_exemplary_related;
    private boolean isSuccessful;

    public String getMsg() {
        return msg;
    }

    public String getLocation_id() {
        return location_id;
    }

    public Integer getTotal_deleted() {
        return total_deleted;
    }

    public Integer getTotal_exemplary_related() {
        return total_exemplary_related;
    }
}
