package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Exemplary;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ExemplaryResponse extends RetrofitResponse<Exemplary> {
    private String msg;
    private String id_exemplary;
    @SerializedName(value = "exemplary", alternate = "exemplary_created")
    private Exemplary exemplary;
    private Date modified;

    private Integer location_quantity_incremented;

    public String getMsg() {
        return msg;
    }

    public String getId_exemplary() {
        return id_exemplary;
    }

    public Integer getLocation_quantity_incremented() {
        return location_quantity_incremented;
    }

    public Date getModified() {
        return modified;
    }

    @Override
    public Exemplary getEntity() {
        return exemplary;
    }
}
