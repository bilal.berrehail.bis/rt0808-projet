package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Item;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ItemResponse extends RetrofitResponse<Item> {
    private String msg;
    private String id_item;
    @SerializedName(value = "item", alternate = "item_created")
    private Item item;
    private Date modified;

    public String getMsg() {
        return msg;
    }

    public String getId_item() {
        return id_item;
    }

    public Date getModified() {
        return modified;
    }

    @Override
    public Item getEntity() {
        return item;
    }
}
