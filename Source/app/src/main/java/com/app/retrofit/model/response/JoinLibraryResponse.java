package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Library;

public class JoinLibraryResponse extends RetrofitResponse<Library> {
    private String msg;

    public String getMsg() {
        return msg;
    }
}
