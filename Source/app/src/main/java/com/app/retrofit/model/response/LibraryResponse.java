package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Library;

public class LibraryResponse extends RetrofitResponse<Library> {
    private Library library;

    @Override
    public Library getEntity() {
        return library;
    }
}
