package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Category;

import java.util.ArrayList;

public class ListCategoryResponse extends PaginationResponse<Category> {
    private ArrayList<Category> categories;

    @Override
    public ArrayList<Category> getEntities() {
        return categories;
    }
}
