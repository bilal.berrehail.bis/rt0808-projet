package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Exemplary;

import java.util.ArrayList;

public class ListExemplaryResponse extends PaginationResponse<Exemplary> {
    private ArrayList<Exemplary> exemplaries;

    @Override
    public ArrayList<Exemplary> getEntities() {
        return exemplaries;
    }
}
