package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Item;

import java.util.ArrayList;

public class ListItemResponse extends PaginationResponse<Item> {
    private ArrayList<Item> items;

    @Override
    public ArrayList<Item> getEntities() {
        return items;
    }
}
