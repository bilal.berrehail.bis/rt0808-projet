package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Library;

import java.util.ArrayList;

public class ListLibraryResponse extends PaginationResponse<Library> {
    private ArrayList<Library> libraries;

    @Override
    public ArrayList<Library> getEntities() {
        return libraries;
    }
}
