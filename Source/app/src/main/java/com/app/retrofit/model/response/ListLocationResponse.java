package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Location;

import java.util.ArrayList;

public class ListLocationResponse extends PaginationResponse<Location> {
    private ArrayList<Location> locations;

    @Override
    public ArrayList<Location> getEntities() {
        return locations;
    }
}
