package com.app.retrofit.model.response;

import com.app.retrofit.model.object.volume.Volume;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListVolumeResponse extends RetrofitResponse<Volume> {
    @SerializedName("items")
    private ArrayList<Volume> volumes;

    @Override
    public ArrayList<Volume> getEntities() {
        return volumes;
    }
}
