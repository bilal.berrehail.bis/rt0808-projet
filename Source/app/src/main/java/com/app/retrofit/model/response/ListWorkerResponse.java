package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Worker;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListWorkerResponse extends PaginationResponse<Worker> {
    private String msg;
    private Integer total_users;
    @SerializedName("users")
    private ArrayList<Worker> workers;

    public String getMsg() {
        return msg;
    }

    public Integer getTotal_users() {
        return total_users;
    }

    @Override
    public ArrayList<Worker> getEntities() {
        return workers;
    }
}