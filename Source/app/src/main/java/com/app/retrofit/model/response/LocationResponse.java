package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Location;
import com.google.gson.annotations.SerializedName;

public class LocationResponse extends RetrofitResponse<Location> {
    private String msg;
    private String id_location;
    @SerializedName(value = "location", alternate = "location_created")
    private Location location;

    public String getMsg() {
        return msg;
    }

    public String getId_location() {
        return id_location;
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public Location getEntity() {
        return location;
    }
}
