package com.app.retrofit.model.response;

import com.app.retrofit.model.object.User;

public class LoginLibraryResponse extends RetrofitResponse<User> {
    private String new_token;
    private String role;

    public String getNew_token() {
        return new_token;
    }

    public String getRole() {
        return role;
    }
}
