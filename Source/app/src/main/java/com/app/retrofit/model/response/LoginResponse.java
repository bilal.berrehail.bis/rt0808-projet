package com.app.retrofit.model.response;

import com.app.retrofit.model.object.User;
import com.google.gson.annotations.SerializedName;

public class LoginResponse extends RetrofitResponse<User> {
    private String msg;
    private User user;

    public String getMsg() {
        return msg;
    }

    @Override
    public User getEntity() {
        return user;
    }
}
