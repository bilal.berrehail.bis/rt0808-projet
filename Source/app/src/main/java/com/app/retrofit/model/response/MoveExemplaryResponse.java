package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Exemplary;

public class MoveExemplaryResponse extends RetrofitResponse {
    private String msg;
    private Exemplary exemplary;
    private String error;

    public String getMsg() {
        return msg;
    }

    public Exemplary getExemplary() {
        return exemplary;
    }

    public String getError() {
        return error;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setExemplary(Exemplary exemplary) {
        this.exemplary = exemplary;
    }

    public void setError(String error) {
        this.error = error;
    }
}
