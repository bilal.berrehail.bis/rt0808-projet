package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Entity;

public class PaginationResponse<T extends Entity> extends RetrofitResponse<T> {
    private Integer total_returned;
    private Integer total_page;

    public Integer getTotal_returned() {
        return total_returned;
    }

    public Integer getTotal_page() {
        return total_page;
    }

    @Override
    public T getEntity() {
        return null;
    }
}
