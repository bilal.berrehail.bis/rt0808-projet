package com.app.retrofit.model.response;

public class RecoveryLendResponse extends RetrofitResponse {
    private String msg;
    private String error;

    public String getMsg() {
        return msg;
    }

    public String getError() {
        return error;
    }
}
