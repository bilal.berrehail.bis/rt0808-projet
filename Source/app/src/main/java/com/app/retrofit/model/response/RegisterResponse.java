package com.app.retrofit.model.response;

import com.app.retrofit.model.object.User;

public class RegisterResponse extends RetrofitResponse<User> {
    private String msg;
    private String error;
    private User user;

    public String getMsg() {
        return msg;
    }

    public String getError() {
        return error;
    }

    @Override
    public User getEntity() {
        return user;
    }
}
