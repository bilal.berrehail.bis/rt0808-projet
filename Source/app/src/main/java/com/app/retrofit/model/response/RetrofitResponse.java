package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Entity;

import java.util.ArrayList;

public class RetrofitResponse<T extends Entity> {
    private boolean is_successful;
    private Integer code_response;

    public T getEntity() {
        return null;
    }

    public ArrayList<T> getEntities() {
        return null;
    }

    public boolean isSuccessful() {
        return is_successful;
    }

    public void setSuccessful(boolean is_successful) {
        this.is_successful = is_successful;
    }

    public Integer getCode_response() {
        return code_response;
    }

    public void setCode_response(Integer code_response) {
        this.code_response = code_response;
    }
}
