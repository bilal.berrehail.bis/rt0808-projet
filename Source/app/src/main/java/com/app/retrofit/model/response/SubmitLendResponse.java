package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Exemplary;

public class SubmitLendResponse extends RetrofitResponse<Exemplary> {
    private String msg;
    private Exemplary exemplary;
    private String error;

    public String getMsg() {
        return msg;
    }

    @Override
    public Exemplary getEntity() {
        return exemplary;
    }

    public String getError() {
        return error;
    }
}
