package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Exemplary;

public class UpdateExemplaryResponse extends RetrofitResponse<Exemplary> {
    private String msg;

    public String getMsg() {
        return msg;
    }
}
