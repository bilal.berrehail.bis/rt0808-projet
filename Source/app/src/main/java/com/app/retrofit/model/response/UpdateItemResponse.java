package com.app.retrofit.model.response;

import com.app.retrofit.model.object.Item;

public class UpdateItemResponse extends RetrofitResponse<Item> {
    private String msg;

    public String getMsg() {
        return msg;
    }
}
