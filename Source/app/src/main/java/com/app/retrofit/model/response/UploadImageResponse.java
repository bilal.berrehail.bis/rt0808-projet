package com.app.retrofit.model.response;

public class UploadImageResponse extends RetrofitResponse {
    private String msg;
    private String path_img;

    public String getMsg() {
        return msg;
    }

    public String getPath_img() {
        return path_img;
    }
}
