package com.app.retrofit.service;

import com.app.retrofit.model.response.CategoryResponse;
import com.app.retrofit.model.response.DeleteCategoryResponse;
import com.app.retrofit.model.response.DeleteExemplaryResponse;
import com.app.retrofit.model.response.DeleteItemResponse;
import com.app.retrofit.model.response.DeleteLocationResponse;
import com.app.retrofit.model.response.ExemplaryResponse;
import com.app.retrofit.model.response.ItemResponse;
import com.app.retrofit.model.response.JoinLibraryResponse;
import com.app.retrofit.model.response.LibraryResponse;
import com.app.retrofit.model.response.ListCategoryResponse;
import com.app.retrofit.model.response.ListExemplaryResponse;
import com.app.retrofit.model.response.ListItemResponse;
import com.app.retrofit.model.response.ListLibraryResponse;
import com.app.retrofit.model.response.ListLocationResponse;
import com.app.retrofit.model.response.ListVolumeResponse;
import com.app.retrofit.model.response.ListWorkerResponse;
import com.app.retrofit.model.response.LocationResponse;
import com.app.retrofit.model.response.LoginLibraryResponse;
import com.app.retrofit.model.response.LoginResponse;
import com.app.retrofit.model.response.MoveExemplaryResponse;
import com.app.retrofit.model.response.RecoveryLendResponse;
import com.app.retrofit.model.response.RegisterResponse;
import com.app.retrofit.model.response.SubmitLendResponse;
import com.app.retrofit.model.response.UpdateExemplaryResponse;
import com.app.retrofit.model.response.UpdateItemResponse;
import com.app.retrofit.model.response.UpdateLibraryResponse;
import com.app.retrofit.model.response.UploadImageResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface APIService {
    @FormUrlEncoded
    @POST("/login")
    Call<LoginResponse> doLogin(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("/register")
    Call<RegisterResponse> doRegister(@FieldMap Map<String, Object> fields);

    @GET("/item")
    Call<ListItemResponse> getItems(@Query("page") int page, @Query("limit") int limit, @Query("keyword") String keyword, @Query("attr") String attr, @Query("value") String value);

    @GET("/item/{id}")
    Call<ItemResponse> getItemById(@Path("id") String id);

    @DELETE("/item/{id}/")
    Call<DeleteItemResponse> deleteItemById(@Path("id") String id);

    @FormUrlEncoded
    @POST("/item")
    Call<ItemResponse> createItem(@FieldMap Map<String, Object> fields);

    @FormUrlEncoded
    @PUT("/item/{id}")
    Call<UpdateItemResponse> updateItemById(@Path("id") String id, @FieldMap Map<String, Object> fields);

    @Multipart
    @POST("/media/upload_image")
    Call<UploadImageResponse> insertImage(@Part MultipartBody.Part img);

    @GET("/exemplary")
    Call<ListExemplaryResponse> getExemplaries(@Query("page") int page, @Query("limit") int limit, @Query("keyword") String keyword);

    @GET("/item/{id}/exemplary")
    Call<ListExemplaryResponse> getExemplariesByItemId(@Path("id") String item_id, @Query("page") int page, @Query("limit") int limit, @Query("keyword") String keyword);

    @GET("/location/{id}/exemplary")
    Call<ListExemplaryResponse> getExemplariesByLocationId(@Path("id") String location_id, @Query("page") int page, @Query("limit") int limit, @Query("keyword") String keyword);

    @GET("/category")
    Call<ListCategoryResponse> getCategories(@Query("page") int page, @Query("limit") int limit, @Query("keyword") String keyword);

    @GET("/category/{id}")
    Call<CategoryResponse> getCategoryById(@Path("id") String id);

    @FormUrlEncoded
    @POST("/category")
    Call<CategoryResponse> createCategory(@FieldMap Map<String, Object> fields);

    @DELETE("/category/{id}")
    Call<DeleteCategoryResponse> deleteCategoryById(@Path("id") String id);

    @GET("/location")
    Call<ListLocationResponse> getLocations(@Query("page") int page, @Query("limit") int limit, @Query("keyword") String keyword);

    @GET("/location/{id}")
    Call<LocationResponse> getLocationById(@Path("id") String id);

    @DELETE("/location/{id}")
    Call<DeleteLocationResponse> deleteLocationById(@Path("id") String id);

    @FormUrlEncoded
    @POST("/location")
    Call<LocationResponse> createLocation(@FieldMap Map<String, Object> fields);

    @FormUrlEncoded
    @POST("/exemplary")
    Call<ExemplaryResponse> createExemplary(@Field("item") String item_id, @Field("location") String location_id, @FieldMap Map<String, Object> fields);

    @DELETE("/exemplary/{id}")
    Call<DeleteExemplaryResponse> deleteExemplary(@Path("id") String id);

    @GET("/exemplary/{id}")
    Call<ExemplaryResponse> getExemplaryById(@Path("id") String id);

    @FormUrlEncoded
    @PUT("/exemplary/{id}")
    Call<UpdateExemplaryResponse> updateExemplaryById(@Path("id") String id, @FieldMap Map<String, Object> fields);

    @FormUrlEncoded
    @POST("/exemplary/{id}/move")
    Call<MoveExemplaryResponse> moveExemplary(@Path("id") String id, @Field("new_location") String new_location_id);

    @FormUrlEncoded
    @POST("/exemplary/{id}/lend")
    Call<SubmitLendResponse> submitExemplaryLend(@Path("id") String id, @Field("receiver_name") String receiver_name, @Field("lend_date") long lend_date, @Field("lend_note") String lend_note);

    @FormUrlEncoded
    @POST("/exemplary/{id}/recover")
    Call<RecoveryLendResponse> recoveryExemplaryLend(@Path("id") String id, @Field("location_id") String location_id);

    @GET
    Call<ListVolumeResponse> getVolumeByISNB(@Url String url);

    @FormUrlEncoded
    @POST("/library")
    Call<LibraryResponse> createLibrary(@FieldMap Map<String, Object> fields);

    @FormUrlEncoded
    @PUT("/library")
    Call<UpdateLibraryResponse> updateLibraryById(@FieldMap Map<String, Object> fields);

    @POST("/library_connect/{id}")
    Call<LoginLibraryResponse> loginLibrary(@Path("id") String id);

    @FormUrlEncoded
    @POST("/library_join/")
    Call<JoinLibraryResponse> joinLibrary(@Field("token_library") String token_library);

    @GET("/library_list")
    Call<ListLibraryResponse> getLibraries(@Query("page") int page, @Query("limit") int limit, @Query("keyword") String keyword);

    @GET("/library_users_list")
    Call<ListWorkerResponse> getLibraryUsers(@Query("page") int page, @Query("limit") int limit, @Query("keyword") String keyword);
}

















