package com.app.retrofit.utils;

import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.response.ListCategoryResponse;
import com.app.retrofit.model.response.ListExemplaryResponse;
import com.app.retrofit.model.response.ListItemResponse;
import com.app.retrofit.model.response.ListLibraryResponse;
import com.app.retrofit.model.response.ListLocationResponse;
import com.app.retrofit.model.response.ListWorkerResponse;
import com.app.utils.CallbackEntity;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;

public class EntityUtils {
    public static final int LIMIT_GET_ITEMS_PER_PAGE = 15;
    public static final int LIMIT_GET_CATEGORIES_PER_PAGE = 15;
    public static final int LIMIT_GET_LOCATIONS_PER_PAGE = 15;
    public static final int LIMIT_GET_EXEMPLARIES_ITEM_PER_PAGE = 15;
    public static final int LIMIT_GET_EXEMPLARIES_LOCATION_PER_PAGE = 15;
    public static final int LIMIT_GET_LIBRARIES_PER_PAGE = 15;
    public static final int LIMIT_GET_USERS_PER_PAGE = 15;

    public static Observable<ListItemResponse> getItemsRx(int page, int limit, String keyword, String attr, String value) {
        return Observable.create((ObservableOnSubscribe<ListItemResponse>) emitter -> {
            Call<ListItemResponse> call = RetrofitInstance.getAPIService().getItems(page, limit, keyword, attr, value);
            call.enqueue(new CallbackEntity<>(ListItemResponse.class, new CallbackEntity.CallbackEntityResponse<ListItemResponse>() {
                @Override
                public void onSuccess(ListItemResponse listItemResponse) {
                    emitter.onNext(listItemResponse);
                    emitter.onComplete();
                }

                @Override
                public void onFailure() {
                    emitter.onError(new Exception());
                }
            }, ResponseCodeHttpConstant.SUCCESS));
        }).subscribeOn(Schedulers.io());
    }

    public static Observable<ListCategoryResponse> getCategoriesRx(int page, int limit, String keyword) {
        return Observable.create((ObservableOnSubscribe<ListCategoryResponse>) emitter -> {
            Call<ListCategoryResponse> call = RetrofitInstance.getAPIService().getCategories(page, limit, keyword);
            call.enqueue(new CallbackEntity<>(ListCategoryResponse.class, new CallbackEntity.CallbackEntityResponse<ListCategoryResponse>() {
                @Override
                public void onSuccess(ListCategoryResponse listCategoryResponse) {
                    emitter.onNext(listCategoryResponse);
                    emitter.onComplete();
                }

                @Override
                public void onFailure() {
                    emitter.onError(new Exception());
                }
            }, ResponseCodeHttpConstant.SUCCESS));
        }).subscribeOn(Schedulers.io());
    }

    public static Observable<ListLocationResponse> getLocationsRx(int page, int limit, String keyword) {
        return Observable.create((ObservableOnSubscribe<ListLocationResponse>) emitter -> {
            Call<ListLocationResponse> call = RetrofitInstance.getAPIService().getLocations(page, limit, keyword);
            call.enqueue(new CallbackEntity<>(ListLocationResponse.class, new CallbackEntity.CallbackEntityResponse<ListLocationResponse>() {
                @Override
                public void onSuccess(ListLocationResponse listLocationResponse) {
                    emitter.onNext(listLocationResponse);
                    emitter.onComplete();
                }

                @Override
                public void onFailure() {
                    emitter.onError(new Exception());
                }
            }, ResponseCodeHttpConstant.SUCCESS));
        }).subscribeOn(Schedulers.io());
    }

    public static Observable<ListExemplaryResponse> getExemplariesRx(int page, int limit, String keyword) {
        return Observable.create((ObservableOnSubscribe<ListExemplaryResponse>) emitter -> {
            Call<ListExemplaryResponse> call = RetrofitInstance.getAPIService().getExemplaries(page, limit, keyword);
            call.enqueue(new CallbackEntity<>(ListExemplaryResponse.class, new CallbackEntity.CallbackEntityResponse<ListExemplaryResponse>() {
                @Override
                public void onSuccess(ListExemplaryResponse listExemplaryResponse) {
                    emitter.onNext(listExemplaryResponse);
                    emitter.onComplete();
                }

                @Override
                public void onFailure() {
                    emitter.onError(new Exception());
                }
            }, ResponseCodeHttpConstant.SUCCESS));
        }).subscribeOn(Schedulers.io());
    }

    public static Observable<ListExemplaryResponse> getExemplariesByItemIdRx(int page, int limit, String keyword, String item_id) {
        return Observable.create((ObservableOnSubscribe<ListExemplaryResponse>) emitter -> {
            Call<ListExemplaryResponse> call = RetrofitInstance.getAPIService().getExemplariesByItemId(item_id, page, limit, keyword);
            call.enqueue(new CallbackEntity<>(ListExemplaryResponse.class, new CallbackEntity.CallbackEntityResponse<ListExemplaryResponse>() {
                @Override
                public void onSuccess(ListExemplaryResponse listExemplaryResponse) {
                    emitter.onNext(listExemplaryResponse);
                    emitter.onComplete();
                }

                @Override
                public void onFailure() {
                    emitter.onError(new Exception());
                }
            }, ResponseCodeHttpConstant.SUCCESS));
        }).subscribeOn(Schedulers.io());
    }

    public static Observable<ListExemplaryResponse> getExemplariesByLocationIdRx(int page, int limit, String keyword, String location_id) {
        return Observable.create((ObservableOnSubscribe<ListExemplaryResponse>) emitter -> {
            Call<ListExemplaryResponse> call = RetrofitInstance.getAPIService().getExemplariesByLocationId(location_id, page, limit, keyword);
            call.enqueue(new CallbackEntity<>(ListExemplaryResponse.class, new CallbackEntity.CallbackEntityResponse<ListExemplaryResponse>() {
                @Override
                public void onSuccess(ListExemplaryResponse listExemplaryResponse) {
                    emitter.onNext(listExemplaryResponse);
                    emitter.onComplete();
                }

                @Override
                public void onFailure() {
                    emitter.onError(new Exception());
                }
            }, ResponseCodeHttpConstant.SUCCESS));
        }).subscribeOn(Schedulers.io());
    }

    public static Observable<ListLibraryResponse> getLibrariesRx(int page, int limit, String keyword) {
        return Observable.create((ObservableOnSubscribe<ListLibraryResponse>) emitter -> {
            Call<ListLibraryResponse> call = RetrofitInstance.getAPIService().getLibraries(page, limit, keyword);
            call.enqueue(new CallbackEntity<>(ListLibraryResponse.class, new CallbackEntity.CallbackEntityResponse<ListLibraryResponse>() {
                @Override
                public void onSuccess(ListLibraryResponse listLibraryResponse) {
                    emitter.onNext(listLibraryResponse);
                    emitter.onComplete();
                }

                @Override
                public void onFailure() {
                    emitter.onError(new Exception());
                }
            }, ResponseCodeHttpConstant.SUCCESS));
        }).subscribeOn(Schedulers.io());
    }

    public static Observable<ListWorkerResponse> getUserOnLibraryRx(int page, int limit, String keyword) {
        return Observable.create((ObservableOnSubscribe<ListWorkerResponse>) emitter -> {
            Call<ListWorkerResponse> call = RetrofitInstance.getAPIService().getLibraryUsers(page, limit, keyword);
            call.enqueue(new CallbackEntity<>(ListWorkerResponse.class, new CallbackEntity.CallbackEntityResponse<ListWorkerResponse>() {
                @Override
                public void onSuccess(ListWorkerResponse listWorkerResponse) {
                    emitter.onNext(listWorkerResponse);
                    emitter.onComplete();
                }

                @Override
                public void onFailure() {
                    emitter.onError(new Exception());
                }
            }, ResponseCodeHttpConstant.SUCCESS));
        }).subscribeOn(Schedulers.io());
    }
}
