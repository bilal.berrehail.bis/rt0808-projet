package com.app.retrofit.utils;

import android.content.Context;

import com.app.R;
import com.app.utils.ViewUtils;

public class ResponseUtils {
    public static void onCallFailed(Context context) {
        ViewUtils.toast(context, R.string.query_failed, true);
    }
}