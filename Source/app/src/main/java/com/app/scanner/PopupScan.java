package com.app.scanner;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.app.R;
import com.app.utils.ViewUtils;
import com.google.zxing.Result;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class PopupScan extends Dialog implements ZXingScannerView.ResultHandler {
    @BindView(R.id.zxscan)
    ZXingScannerView zxScan;
    @BindView(R.id.btn_back)
    Button btnBack;

    private final Context context;

    @SuppressLint("StaticFieldLeak")
    private static PopupScan popupScan;
    private final PopupScanListener popupScanListener;

    private PopupScan(Context context, PopupScanListener popupScanListener) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
        this.popupScanListener = popupScanListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_scan, null);

        ButterKnife.bind(this, view);

        setContentView(view);
        initView();
    }

    @Override
    public void dismiss() {
        zxScan.stopCamera();
        popupScan = null;
        super.dismiss();
    }

    public static PopupScan getInstance(Context context, PopupScanListener popupScanListener) {
        if (popupScan == null) {
            popupScan = new PopupScan(context, popupScanListener);
        }
        return popupScan;
    }

    private void initView() {
        Dexter.withContext(context)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                        zxScan.setResultHandler(PopupScan.this);
                        zxScan.startCamera();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
                        ViewUtils.toast(context, R.string.deny_camera_access, true);
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {

                    }
                }).check();

        btnBack.setOnClickListener(v -> dismiss());
    }

    @Override
    public void handleResult(Result rawResult) {
        popupScanListener.onResult(rawResult);
        zxScan.resumeCameraPreview(this);
    }

    public interface PopupScanListener {
        void onResult(Result result);
    }
}
