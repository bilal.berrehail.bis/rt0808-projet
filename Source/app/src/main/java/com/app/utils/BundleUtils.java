package com.app.utils;

import android.os.Bundle;

public class BundleUtils {
    public static Object getValue(Bundle bundle, String key) {
        if (bundle != null) {
            if (bundle.containsKey(key)) {
                return bundle.get(key);
            }
        }
        return null;
    }
}
