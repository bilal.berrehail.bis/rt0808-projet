package com.app.utils;

import androidx.annotation.NonNull;

import com.app.retrofit.model.response.RetrofitResponse;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallbackEntity<T extends RetrofitResponse> implements Callback<T> {
    private final List<Integer> successCodes;
    private final Class<T> classUsed;

    private final CallbackEntityResponse<T> callbackEntityResponse;

    public CallbackEntity(Class<T> classUsed, CallbackEntityResponse<T> callbackEntityResponse, Integer... successCodes) {
        this.classUsed = classUsed;
        this.callbackEntityResponse = callbackEntityResponse;
        this.successCodes = Arrays.asList(successCodes);
    }

    @Override
    public void onResponse(@NonNull Call<T> call, Response<T> response) {
        if (successCodes.contains(response.code())) {
            if (response.body() != null) {
                response.body().setCode_response(response.code());
                response.body().setSuccessful(response.isSuccessful());
                callbackEntityResponse.onSuccess(response.body());
                return;
            } else if (response.errorBody() != null) {
                try {
                    T retrofitResponse = new Gson().fromJson(response.errorBody().string(), classUsed);
                    retrofitResponse.setCode_response(response.code());
                    retrofitResponse.setSuccessful(response.isSuccessful());
                    callbackEntityResponse.onSuccess(retrofitResponse);
                    return;
                } catch (IOException ignored) {
                }
            }
        }
        callbackEntityResponse.onFailure();
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        callbackEntityResponse.onFailure();
    }

    public interface CallbackEntityResponse<T extends RetrofitResponse> {
        void onSuccess(T retrofitResponse);

        void onFailure();
    }
}
