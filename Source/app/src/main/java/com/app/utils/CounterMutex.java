package com.app.utils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class CounterMutex extends LiveData {
    private MutableLiveData<Integer> mCurrentCounter;

    public CounterMutex() {
        mCurrentCounter = new MutableLiveData<>();
        mCurrentCounter.setValue(0);
    }

    public MutableLiveData<Integer> getCurrentCounter() {
        if (mCurrentCounter == null) {
            mCurrentCounter = new MutableLiveData<>();
        }
        return mCurrentCounter;
    }

    public void incrementCounter() {
        if (mCurrentCounter != null) {
            if (mCurrentCounter.getValue() != null) {
                mCurrentCounter.setValue(mCurrentCounter.getValue() + 1);
            }
        }
    }
}
