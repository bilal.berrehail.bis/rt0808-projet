package com.app.utils;

import com.app.model.constant.ResponseCodeHttpConstant;
import com.app.retrofit.instance.RetrofitInstance;
import com.app.retrofit.model.object.Exemplary;
import com.app.retrofit.model.response.ExemplaryResponse;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

import retrofit2.Call;

public class DataScanUtils {

    public static void buildExemplary(String qrCodeContent, DataScanUtilsListener dataScanUtilsListener) {
        Type mapType = new TypeToken<Map<String, String>>() {
        }.getType();
        try {
            Map<String, String> mapResult = new Gson().fromJson(qrCodeContent, mapType);
            if (mapResult.containsKey("exemplary_id")) {
                String exemplary_id = mapResult.get("exemplary_id");

                Call<ExemplaryResponse> call = RetrofitInstance.getAPIService().getExemplaryById(exemplary_id);
                call.enqueue(new CallbackEntity<>(ExemplaryResponse.class, new CallbackEntity.CallbackEntityResponse<ExemplaryResponse>() {
                    @Override
                    public void onSuccess(ExemplaryResponse exemplaryResponse) {
                        Exemplary exemplary = exemplaryResponse.getEntity();
                        dataScanUtilsListener.onBuildExemplary(exemplary);
                    }

                    @Override
                    public void onFailure() {
                        dataScanUtilsListener.onBuildFailed();

                    }
                }, ResponseCodeHttpConstant.SUCCESS));

            } else {
                dataScanUtilsListener.onBuildFailed();
            }
        } catch (JsonSyntaxException ex) {
            dataScanUtilsListener.onBuildFailed();
        }
    }

    public interface DataScanUtilsListener {
        void onBuildExemplary(Exemplary exemplary);

        void onBuildFailed();
    }
}
