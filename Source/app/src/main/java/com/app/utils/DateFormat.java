package com.app.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormat {
    public static String formatSimpleFormat(Date date) {
        if (date == null) date = new Date(0);
        return new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ROOT).format(date);
    }
}
