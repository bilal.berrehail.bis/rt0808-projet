package com.app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import com.app.model.constant.SharedPreferencesConstant;
import com.app.model.constant.SharedPreferencesItemConstant;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class LanguageUtils {
    public static final String FRANCE = "fr";
    public static final String ENGLISH = "en";

    public static void changeLanguage(Context context, String language) {
        Locale locale = new Locale(language);
        Configuration config = new Configuration(context.getResources().getConfiguration());
        Locale.setDefault(locale);
        config.setLocale(locale);

        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());

        SharedPreferences sharedPreferencesLanguage = context.getSharedPreferences(SharedPreferencesConstant.LANGUAGE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferencesLanguage.edit();
        editor.putString(SharedPreferencesItemConstant.PREF_LANGUAGE, language);
        editor.apply();
    }

    public static void loadPrefLanguage(Context context) {
        SharedPreferences sharedPreferencesLanguage = context.getSharedPreferences(SharedPreferencesConstant.LANGUAGE, MODE_PRIVATE);
        if (sharedPreferencesLanguage.contains(SharedPreferencesItemConstant.PREF_LANGUAGE)) {
            changeLanguage(context, sharedPreferencesLanguage.getString(SharedPreferencesItemConstant.PREF_LANGUAGE, "en"));
        }
    }
}
