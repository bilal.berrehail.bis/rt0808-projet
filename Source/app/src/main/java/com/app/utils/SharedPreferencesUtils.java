package com.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.model.constant.SharedPreferencesConstant;
import com.app.model.constant.SharedPreferencesItemConstant;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferencesUtils {
    public static void uploadCredentials(Context context, String email, String password) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SharedPreferencesConstant.LOGIN, MODE_PRIVATE);
        SharedPreferences.Editor editorSP = sharedPreferences.edit();

        editorSP.putString(SharedPreferencesItemConstant.EMAIL, email);
        editorSP.putString(SharedPreferencesItemConstant.PASSWORD, password);

        editorSP.apply();
    }

    public static void deleteCredentials(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SharedPreferencesConstant.LOGIN, MODE_PRIVATE);
        if (sharedPreferences.contains(SharedPreferencesItemConstant.EMAIL) &&
                sharedPreferences.contains(SharedPreferencesItemConstant.PASSWORD)) {
            SharedPreferences.Editor editorSP = sharedPreferences.edit();
            editorSP.clear();
            
            editorSP.apply();
        }
    }
}
