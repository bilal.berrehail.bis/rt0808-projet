package com.app.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class StorageUtils {

    public static File moveFileToLocalDirectory(Context context ,Uri uri) {
        File file;
        try {
            file = StorageUtils.createImageFile(context);
        } catch (IOException ex) {
            return null;
        }

        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            FileOutputStream fileOutputStream = new FileOutputStream(file);

            if (inputStream == null) return null;

            StorageUtils.copyStream(inputStream, fileOutputStream);
            fileOutputStream.close();
            inputStream.close();
        } catch (IOException ex) {
            return null;
        }

        return file;
    }

    private static File createImageFile(Context context) throws IOException {
        long timeStamp = System.currentTimeMillis();
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        return File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
    }

    private static void copyStream(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public static void writeBitmap(Context context, Bitmap bitmap, String title) {
        MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, title , "");
    }
}
