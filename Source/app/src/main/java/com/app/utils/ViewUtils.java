package com.app.utils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;

import androidx.recyclerview.widget.RecyclerView;

import com.app.R;
import com.app.model.decoration.MarginItemDecoration;
import com.app.view.PaginationAdapter;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.squareup.picasso.Picasso;

import java.util.Calendar;

public class ViewUtils {
    public static String getString(String str) {
        if (str != null) {
            return str;
        }
        return "-";
    }

    public static PopupWindow createPopupMenu(Context context, final View anchor, RecyclerView.Adapter adapter, RecyclerView.LayoutManager layoutManager) {
        final PopupWindow popupWindow = new PopupWindow(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (inflater == null) return null;

        final View contain = inflater.inflate(R.layout.popup_menu_generic, null);

        EditText etSearchBar = contain.findViewById(R.id.et_search_bar);
        RecyclerView rvObject = contain.findViewById(R.id.rv_object);

        if (adapter instanceof PaginationAdapter) {
            etSearchBar.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    ((PaginationAdapter) adapter).refreshKeyword(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        } else {
            etSearchBar.setVisibility(View.GONE);
        }

        rvObject.setLayoutManager(layoutManager);
        rvObject.addItemDecoration(new MarginItemDecoration(4));
        rvObject.setAdapter(adapter);

        popupWindow.setFocusable(true);
        popupWindow.setWidth(anchor.getMeasuredWidth());
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(contain);

        return popupWindow;
    }

    public static void loadImage(ImageView imageView, String url) {
        if (!TextUtils.isEmpty(url)) {
            Picasso.get().load(url).error(R.drawable.img_img_not_found).into(imageView);
        } else {
            imageView.setImageResource(R.drawable.img_img_not_found);
        }
    }

    public static void loadImage(ImageView imageView, Uri uri) {
        if (uri != null) {
            Picasso.get().load(uri).error(R.drawable.img_img_not_found).into(imageView);
        } else {
            imageView.setImageResource(R.drawable.img_img_not_found);
        }
    }

    public static Bitmap generateQrCode(String content, String label) {
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, 512, 512);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }

            int extraHeight = (int) (bmp.getHeight() * 0.2);

            Bitmap newBitmap = Bitmap.createBitmap(bmp.getWidth(),
                    bmp.getHeight() + extraHeight, Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(newBitmap);
            canvas.drawColor(Color.WHITE);
            canvas.drawBitmap(bmp, 0, 0, null);

            Paint pText = new Paint();
            pText.setColor(Color.BLACK);

            setTextSizeForWidth(pText, (int) (bmp.getHeight() * 0.10), label);

            Rect bounds = new Rect();
            pText.getTextBounds(label, 0, label.length(), bounds);

            int x = ((newBitmap.getWidth() - (int) pText.measureText(label)) / 2);
            int h = (extraHeight + bounds.height()) / 2;
            int y = (bmp.getHeight() + h);

            canvas.drawText(label, x, y, pText);

            return newBitmap;
        } catch (WriterException ignored) {

        }
        return null;
    }

    private static void setTextSizeForWidth(Paint paint, float desiredHeight, String text) {
        final float testTextSize = 48f;

        paint.setTextSize(testTextSize);
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);

        float desiredTextSize = testTextSize * desiredHeight / bounds.height();

        paint.setTextSize(desiredTextSize);
    }

    public static void showDateTimePicker(Context context, PickDateListener pickDateListener) {
        final Calendar currentDate = Calendar.getInstance();
        Calendar pickDate = Calendar.getInstance();
        new DatePickerDialog(context, (view, year, monthOfYear, dayOfMonth) -> {
            pickDate.set(year, monthOfYear, dayOfMonth);
            new TimePickerDialog(context, (view1, hourOfDay, minute) -> {
                pickDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                pickDate.set(Calendar.MINUTE, minute);
                pickDateListener.onPick(pickDate);
            }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public static void hideKeyboard(Window window) {
        if (window != null) {
            View focused = window.getCurrentFocus();
            if (focused != null) {
                InputMethodManager imm = (InputMethodManager) focused.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(focused.getWindowToken(), 0);
                }
            }
        }
    }

    public static void toast(Context context, int resId, boolean isError) {
        StyleableToast.makeText(context, context.getString(resId), isError ? R.style.errorToast : R.style.successToast).show();
    }

    public static void toast(Context context, String msg, boolean isError) {
        StyleableToast.makeText(context, msg, isError ? R.style.errorToast : R.style.successToast).show();
    }

    public interface PickDateListener {
        void onPick(Calendar calendar);
    }
}





