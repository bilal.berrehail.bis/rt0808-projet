package com.app.view;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.app.utils.ViewUtils;

public class BindingAdapterView {
    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView imageView, String url) {
        ViewUtils.loadImage(imageView, url);
    }
}
