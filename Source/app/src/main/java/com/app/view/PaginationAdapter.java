package com.app.view;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.dialog.helper.DialogHelper;
import com.app.retrofit.model.object.Entity;
import com.app.retrofit.model.response.PaginationResponse;
import com.app.retrofit.utils.ResponseUtils;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class PaginationAdapter<T extends PaginationResponse<?>, E extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<E> implements Observer<T> {
    protected abstract Observable<T> getObservable();

    protected final ArrayList<Entity> entities;

    private final SwipeRefreshLayout swipeRefreshLayout;
    private Observer<T> observer;

    private ArrayList<Subscriber> subscribers;

    private final Context context;

    private final CompositeDisposable disposables = new CompositeDisposable();

    protected int currentPage = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    protected String keyword;

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        disposables.clear();
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setOnRefreshListener(null);
        }
        recyclerView.clearOnScrollListeners();
    }

    protected PaginationAdapter(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        this.entities = new ArrayList<>();
        this.context = context;
        this.swipeRefreshLayout = swipeRefreshLayout;
        this.subscribers = new ArrayList<>();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        if (recyclerView.getLayoutManager() != null) {
            if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                recyclerView.addOnScrollListener(new PaginationListener((LinearLayoutManager) recyclerView.getLayoutManager()) {
                    @Override
                    protected void loadMoreItems() {
                        currentPage++;
                        getResources();
                    }

                    @Override
                    public boolean isLastPage() {
                        return isLastPage;
                    }

                    @Override
                    public boolean isLoading() {
                        return isLoading;
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return entities.size();
    }

    public void subscribe(Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    public boolean remove(Entity entity) {
        if (entities.contains(entity)) {
            boolean isRemoved = entities.remove(entity);
            notifyDataSetChanged();
            return isRemoved;
        }
        return false;
    }

    protected void start(Observer<T> observer) {
        this.observer = observer;

        getResources();
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setOnRefreshListener(this::scratchResources);
        }
    }

    private void getResources() {
        DialogHelper.getInstance().showLoading(context);
        isLoading = true;
        getObservable().observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    private void scratchResources() {
        currentPage = 1;
        isLastPage = false;
        getResources();
    }

    public void refreshKeyword(String keyword) {
        this.keyword = keyword;
        scratchResources();
    }

    @Override
    public void onSubscribe(Disposable d) {
        disposables.add(d);
    }

    @Override
    public void onNext(T t) {
        onNext(t, t.getEntities());
    }

    protected void onNext(PaginationResponse<?> paginationResponse, ArrayList<? extends Entity> entities) {
        DialogHelper.getInstance().dismissDialog();
        for (Subscriber subscriber : subscribers) {
            subscriber.onResponse(paginationResponse);
        }
        if (paginationResponse.getTotal_page() != null && paginationResponse.getTotal_page() == currentPage) {
            isLastPage = true;
        }
        isLoading = false;
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
        if (currentPage == 1) {
            this.entities.clear();
        }
        if (entities != null) {
            this.entities.addAll(entities);
        }

        notifyDataSetChanged();
    }

    @Override
    public void onComplete() {
    }

    @Override
    public void onError(Throwable e) {
        DialogHelper.getInstance().dismissDialog();
        isLoading = false;
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
        ResponseUtils.onCallFailed(context);
    }

    public interface Subscriber {
        void onResponse(PaginationResponse<?> paginationResponse);
    }
}
