package com.app.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.app.R;
import com.app.utils.StorageUtils;
import com.app.utils.ViewUtils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PopupBitmap extends Dialog {
    @BindView(R.id.iv_bitmap)
    ImageView ivBitmap;
    @BindView(R.id.btn_back)
    Button btnBack;
    @BindView(R.id.btn_download)
    Button btnDownload;

    private final Context context;

    private final Bitmap bitmap;

    @SuppressLint("StaticFieldLeak")
    private static PopupBitmap popupBitmap;

    private PopupBitmap(Context context, Bitmap bitmap) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
        this.bitmap = bitmap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_bitmap, null);

        ButterKnife.bind(this, view);

        setContentView(view);
        initView();
    }

    @Override
    public void dismiss() {
        popupBitmap = null;
        super.dismiss();
    }

    private void initView() {
        ivBitmap.setImageBitmap(bitmap);
        btnBack.setOnClickListener(v -> dismiss());
        btnDownload.setOnClickListener(v -> downloadBitmap());
    }

    private void downloadBitmap() {
        Dexter.withContext(context)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                        StorageUtils.writeBitmap(context, bitmap, String.valueOf(System.currentTimeMillis()));
                        ViewUtils.toast(context, R.string.download_success, false);
                        dismiss();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
                        ViewUtils.toast(context, R.string.deny_writing_external_storage_access, true);
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {

                    }
                }).check();
    }

    public static PopupBitmap getInstance(Context context, Bitmap bitmap) {
        if (popupBitmap == null) {
            popupBitmap = new PopupBitmap(context, bitmap);
        }
        return popupBitmap;
    }
}
