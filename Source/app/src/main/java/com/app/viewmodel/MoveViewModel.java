package com.app.viewmodel;

import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModel;

import com.app.retrofit.model.object.Item;
import com.app.retrofit.model.object.Location;
import com.app.retrofit.model.object.Exemplary;

public class MoveViewModel extends ViewModel {
    private final ObservableField<Item> item;
    private final ObservableField<Exemplary> exemplary;
    private final ObservableField<Location> locationArrival;

    public MoveViewModel() {
        this.item = new ObservableField<>();
        this.exemplary = new ObservableField<>();
        this.locationArrival = new ObservableField<>();
    }

    public ObservableField<Item> getItem() {
        return item;
    }

    public ObservableField<Exemplary> getExemplary() {
        return exemplary;
    }

    public ObservableField<Location> getLocationArrival() {
        return locationArrival;
    }

    public void setItem(Item item) {
        this.item.set(item);
    }

    public void setExemplary(Exemplary exemplary) {
        this.exemplary.set(exemplary);
    }

    public void setLocationArrival(Location locationArrival) {
        this.locationArrival.set(locationArrival);
    }
}
