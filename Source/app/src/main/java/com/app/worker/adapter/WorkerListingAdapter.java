package com.app.worker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.R;
import com.app.databinding.ObjectWorkerListingBinding;
import com.app.retrofit.model.object.Worker;
import com.app.retrofit.model.response.ListWorkerResponse;
import com.app.retrofit.utils.EntityUtils;
import com.app.view.PaginationAdapter;
import com.app.worker.holder.WorkerListingHolder;

import io.reactivex.Observable;

public class WorkerListingAdapter extends PaginationAdapter<ListWorkerResponse, WorkerListingHolder> {
    public WorkerListingAdapter(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        super(context, swipeRefreshLayout);

        start(this);
    }

    @NonNull
    @Override
    public WorkerListingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ObjectWorkerListingBinding objectWorkerListingBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.object_worker_listing, parent, false);
        return new WorkerListingHolder(objectWorkerListingBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull WorkerListingHolder holder, int position) {
        holder.bind((Worker) entities.get(position));
    }

    @Override
    protected Observable<ListWorkerResponse> getObservable() {
        return EntityUtils.getUserOnLibraryRx(currentPage, EntityUtils.LIMIT_GET_USERS_PER_PAGE, keyword);
    }
}
