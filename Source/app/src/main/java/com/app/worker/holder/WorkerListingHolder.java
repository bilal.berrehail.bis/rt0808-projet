package com.app.worker.holder;

import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.R;
import com.app.databinding.ObjectWorkerListingBinding;
import com.app.model.constant.EnumRole;
import com.app.retrofit.model.object.Worker;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WorkerListingHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_worker)
    ImageView ivWorker;

    private final ObjectWorkerListingBinding objectWorkerListingBinding;

    public WorkerListingHolder(ObjectWorkerListingBinding objectWorkerListingBinding) {
        super(objectWorkerListingBinding.getRoot());
        ButterKnife.bind(this, objectWorkerListingBinding.getRoot());
        this.objectWorkerListingBinding = objectWorkerListingBinding;
    }

    public void bind(Worker worker) {
        if (worker.getUser() != null) {
            EnumRole enumRole = EnumRole.getEnumRoleFromRole(worker.getRole());
            if (enumRole != null) {
                worker.getUser().setRole(enumRole);
                if (worker.getUser().getRole() == EnumRole.ADMIN) {
                    ivWorker.setImageDrawable(itemView.getContext().getDrawable(R.drawable.img_crown));
                }
            }
            objectWorkerListingBinding.setUser(worker.getUser());
        }
    }
}
