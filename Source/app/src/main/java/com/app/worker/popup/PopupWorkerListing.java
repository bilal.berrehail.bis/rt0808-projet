package com.app.worker.popup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.R;
import com.app.model.decoration.MarginItemDecoration;
import com.app.retrofit.model.response.ListWorkerResponse;
import com.app.worker.adapter.WorkerListingAdapter;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PopupWorkerListing extends Dialog {
    @BindView(R.id.tv_total_users)
    TextView tvTotalUsers;
    @BindView(R.id.rv_users)
    RecyclerView rvUsers;
    @BindView(R.id.btn_back)
    Button btnBack;

    private final Context context;

    @SuppressLint("StaticFieldLeak")
    private static PopupWorkerListing popupWorkerListing;

    private PopupWorkerListing(Context context) {
        super(context, R.style.AppTheme_PopUpThem);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(R.color.semi_transparent);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_worker_listing, null);

        ButterKnife.bind(this, view);
        setContentView(view);

        initView();
    }

    @Override
    public void dismiss() {
        popupWorkerListing = null;
        rvUsers.setAdapter(null);
        super.dismiss();
    }

    public static PopupWorkerListing getInstance(Context context) {
        if (popupWorkerListing == null) {
            popupWorkerListing = new PopupWorkerListing(context);
        }
        return popupWorkerListing;
    }

    private void initView() {
        WorkerListingAdapter workerListingAdapter = new WorkerListingAdapter(context, null);

        workerListingAdapter.subscribe(paginationResponse -> {

            if (paginationResponse instanceof ListWorkerResponse) {
                ListWorkerResponse listWorkerResponse = (ListWorkerResponse) paginationResponse;
                if (listWorkerResponse.getTotal_users() != null) {
                    tvTotalUsers.setText(String.valueOf(((ListWorkerResponse) paginationResponse).getTotal_users()));
                }
            }

        });

        rvUsers.setLayoutManager(new LinearLayoutManager(context));
        rvUsers.addItemDecoration(new MarginItemDecoration(8));
        rvUsers.setAdapter(workerListingAdapter);

        btnBack.setOnClickListener(v -> dismiss());
    }
}
